﻿using UnityEngine;
using System.Collections;

namespace U2DProjectCore { 
	/**
	@brief Handles collection of input events and associated logic 

	Part of Project Core unity package.
	This input controller is designed to manage the limited inputs used in the 
	Project Core template. More complete input controllers will be implemented in
	specific supplimental packages and will be tuned to those specific game types. 
	The logic in here may be incorporated into the updated controller, but may also
	be left separate and will still function.
	Input Controllers should be components of Scene Master, since the input modes
	may be different depending on the scene's requirements.

	@author Jonny Aiello
	@version 0.1.1
	@date 3/31/16
	*/ 
	public class InputController_core : MonoBehaviour {

		// Reference Variables
		private TimeController timeController; 

		// [[ ----- START ----- ]]
		void Start () {
			timeController = GameObject.Find("[GameMaster]")
				.GetComponent<TimeController>(); 
		}
		
		// [[ ----- UPDATE ----- ]]
		void Update () {
		
			if( Input.GetKeyDown(KeyCode.Escape) ){
				if( Time.timeScale > 0 ){ timeController.SetTimePaused(); }
				else{ timeController.SetTimeNormal(); } 
			}
		}
	}
}