﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace U2DProjectCore {  
	/**
	@brief An in-game timer 

	Part of Project Core unity package.
	Contains public methods for starting, stopping and resetting countdown timer. 
	The countdown timer saves and prints time remaining in whole seconds, in a 
	single row of digits without punctuation or minute count. Logic executed if 
	timer reaches zero must be added, along with references to external scripts 
	triggered. 

	_Class output:_
	<br> none

	_Class input:_
	<br> Checks state of TimeController to see if it is paused
	<br> Classes that would affect start/pause or resetting the timer

	@author Jonny Aiello
	@version 0.1.1
	@date 3/24/16
	*/ 
	public class TimerScript : MonoBehaviour {

		// Variables
		public float totalTime;
		public string prefixText = "Timer: "; 
		public string timerDigits = "000";
		private float remainingTime; 
		private float lastSecond; // in timer unpause, lastSecond = Time.time
		private bool timerIsRunning;
		private bool timeOver; 
		
		// Reference Variables
		public Text timeText; 

		// Properties
		public bool TimerIsRunning { get{return timerIsRunning;} }

	// -----------------------------------------------------------------------------
	// Unity Events

		// [[ ----- START ----- ]]
		void Start () {
			// init timer
			remainingTime = totalTime; 
			UpdateText(); 
		}

		// [[ ----- ON ENABLE ----- ]]
		private void OnEnable(){
			TimeController.OnTimePaused += PauseTimer;
			TimeController.OnTimeNormal += StartTimer;
		}

		// [[ ----- ON DISABLE ----- ]]
		private void OnDisable(){
			TimeController.OnTimePaused -= PauseTimer;
			TimeController.OnTimeNormal -= StartTimer; 
		}

		// [[ ----- UPDATE ----- ]]
		void Update () {

			// update timer
			if( timerIsRunning ){ UpdateTimer(); }
		}

	// -----------------------------------------------------------------------------
	// Private Methods

		// [[ ----- UPDATE TIMER ----- ]]
		private void UpdateTimer(){
			if( Time.time - lastSecond >= 1 ){
				if( remainingTime > 0 ){
					remainingTime--;
					UpdateText();
				}
				else if( remainingTime == 0 && !timeOver ){ 
					UpdateText();
					OutOfTime();
				} 

				lastSecond = Time.time; 
			}
		}

		// [[ ----- UPDATE TEXT ----- ]]
		private void UpdateText(){
			if( timeText != null ){ 
				timeText.text = prefixText + remainingTime.ToString(timerDigits);
			}
		}

		/**@brief If timer ending triggers specific logic, activate here */ 
		// [[ ----- OUT OF TIME ----- ]]
		private void OutOfTime(){
			timeOver = true;
			timerIsRunning = false; 
			/*Debug.Log("Timer: Time's Up");*/
		}

	// -----------------------------------------------------------------------------
	// Public Methods
		/**@brief start or unpause timer */ 
		// [[ ----- START TIMER ----- ]]
		public void StartTimer(){
			if( remainingTime > 0 ){ 
				timeOver = false; 
				timerIsRunning = true; 
				lastSecond = Time.time;
				/*Debug.Log("Timer: started"); */
			}
			else{ Debug.LogWarning("TimerScript: no time remaining, can't start!");}
		}
		/**@brief stop timer progression without changing remaining time */ 
		// [[ ----- PAUSE TIMER ----- ]]
		public void PauseTimer(){
			timerIsRunning = false; 
			/*Debug.Log("Timer: paused");*/
		}

		/**@brief set totalTime value then reset timer*/ 
		// [[ ----- RESET TIMER ----- ]]
		public void ResetTimer( float _totalTime ){
			timeOver = false;
			timerIsRunning = false; 
			totalTime = _totalTime; 
			remainingTime = totalTime; 
			UpdateText();
			/*Debug.Log("Timer: reset");*/
		}
		/**@brief reset timer to totalTime */ 
		public void ResetTimer(){
			timeOver = false;
			timerIsRunning = false;
			remainingTime = totalTime;
			UpdateText();
			/*Debug.Log("Timer: reset");*/
		}
	}
}