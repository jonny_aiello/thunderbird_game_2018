﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

namespace U2DProjectCore { 
	/**
	@brief Holds and updates scores for the duration of the game 

	Part of Project Core unity package.<br>
	This script is static - it does NOT derive from MonoBehaviour and is not a 
	component of any GameObject. Only one ScoreController is needed per game and so 
	it is not instantiated. If a need arises for more scores to be tracked, these 
	scores should be added to the "scores" static dictionary.<br>
	Handles the saving and loading of scores from scoreData.db - contains methods 
	for this that should be called at appropriate times, ex. when quitting the game,
	save current score, when starting the game, load the current high score. <br>
	Whenever a score is updated an event is triggered. This event is subscribed to
	by ScoreGuiScript, which updates an on-screen score counter with the new number. 
	Score values are read and set through public methods. Because this class
	is globally addressable, each of these methods has a string _caller arg that 
	should be passed the name of the script which is calling the method. This helps 
	track calls during debugging. 

	_Class output:_
	<br> Triggers an event every time a score is updated
	<br> Calls DataService class when reading or writing to the database

	_Class input:_
	<br> Scripts which get or modify game scores

	@author Jonny Aiello
	@version 0.1.1
	@date 3/30/16
	*/ 

	public class ScoreController {

		// Variables
		private static Dictionary<string, int> scores 
			= new Dictionary<string, int>{
				{"LvlScore", 0},
				{"HighScore", 0}
			};
		private static int maxEntries = 50; 
			/**< determines how many scores to hold in db before deleting lowest */

		// Event Setup
		public delegate void ScoreChanged();
		public static event ScoreChanged OnScoreChanged;

	// -----------------------------------------------------------------------------
	// Database Methods

		/*
		// [[ ----- SAVE SCORE TO DB ----- ]]
		public static void SaveScoreToDB(){
			var ds = new DataService("scoreData.db");

			// if hit maxEntries, remove a score first
			int allScoresLength = GetDBLength( ds ); 
			if( allScoresLength >= maxEntries ){ DeleteLowestScoreFromDB( ds ); }

			ds.CreateScore( scores["LvlScore"] ); 
			Debug.Log("ScoreController: Score saved to database" + null);
		}

		// [[ ----- DELETE LOWEST SCORE FROM DB ----- ]]
		private static void DeleteLowestScoreFromDB( DataService _ds ){
			var allScores = _ds.GetScores(); 
			float lowPoints = Mathf.Infinity;
			Score lowScore = null; 
				
			// iterate scores until lowest score found
			foreach( var s in allScores ){
				if( s.Points < lowPoints ){ 
					lowPoints = s.Points; 
					lowScore = s; 
				}
			}

			_ds.DeleteScore( lowScore ); 
		}

		// [[ ----- GET DB LENGTH ----- ]]
		private static int GetDBLength( DataService _ds ){
			int dbl = 0;
			var allScores = _ds.GetScores();
			foreach( var s in allScores ){ 
				if( s != null ){ dbl++; } 
			}
			return dbl; 
		}

		// [[ ----- LOAD SCORES FROM DB ----- ]]
		public static IEnumerable<Score> LoadScoresFromDB(){
			var ds = new DataService("scoreData.db");
			return ds.GetScores(); 
		}

		// [[ ----- LOAD HIGH SCORE FROM DB ----- ]]
		public static void LoadHighScoreFromDB(){
			var ds = new DataService("scoreData.db"); 
			var dbScores = ds.GetScores(); 

			// pull and sort data
			int highScore = 0; 
			foreach( var s in dbScores ){
				if( s.Points > highScore ){ highScore = s.Points; }
			}

			// set high score
			SetScore( "HighScore", highScore, "ScoreController"); 
		}
		*/

	// -----------------------------------------------------------------------------
	// Public Methods
		/**@brief Increments or decrements score by pos or neg value provided */ 
		// [[ ----- UPDATE SCORE ----- ]]
		public static void UpdateScore(string _name, int _val, string _caller){
			if( scores.ContainsKey(_name) ){ 
				
				scores[_name] += _val;
				CheckHighscore();
				
				// Call event
				if( OnScoreChanged != null ){ OnScoreChanged(); }

				/*Debug.Log(System.String.Format(
					"{0} score UPDATED by: {1} new score: {2}", 
					_name, _caller, scores[_name]));*/

			}
			else{ Debug.LogWarning("ScoreController: name/int arg is invalid!!"); }
		}
		/**@brief sets specified score to a passed int */ 
		// [[ ----- SET SCORE ----- ]]
		public static void SetScore( string _name, int _val, string _caller ){
			if( scores.ContainsKey(_name) ){ 
				
				scores[_name] = _val; 
				CheckHighscore();
				
				// Call event
				if( OnScoreChanged != null ){ OnScoreChanged(); }
					
				/*Debug.Log(System.String.Format(
					"{0} score SET by: {1} new score: {2}", 
					_name, _caller, scores[_name]));*/
			}
			else{ Debug.LogWarning("ScoreController: name/int arg is invalid!!"); }
		}
		/**@brief return specified score */ 
		// [[ ----- GET SCORE ----- ]]
		public static int GetScore( string _name ){
			if( scores.ContainsKey(_name) ){ return scores[_name]; }
			else{
				Debug.LogWarning("ScoreController: name not found in scores!!");
			}
			return 0;
		}
		/**@brief if the LvlScore is more than the HighScore, update HighScore */ 
		// [[ ----- CHECK HIGHSCORE ----- ]]
		public static void CheckHighscore(){
			if( scores["LvlScore"] > scores["HighScore"] ){
				SetScore( "HighScore", scores["LvlScore"], "ScoreController" ); 
			}
		}
	}
}