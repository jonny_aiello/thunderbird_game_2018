﻿using UnityEngine;
using System.Collections;

namespace U2DProjectCore { 
	/**
	@brief Manages the organization of pooled spawnable objects 

	Part of Project Core unity package.
	Contains an array of pooled objects and an array of spawn points. Contains 
	public methods for sending the next available pooled object to a specific 
	spawn point or a random one.

	_Class output:_
	<br> none

	_Class input:_
	<br> Any object that affects the interval or location of a spawned object, or 
	triggers an object to spawn

	@author Jonny Aiello
	@version 0.1.1
	@date 3/29/16
	*/ 
	public class SpawnController : MonoBehaviour {

		// Variables
		public bool debugOn; 
		public bool automatic; 
			/**< if on will automatically spawn at set rate */
		public float autoDelay = 1f;
		public float SpawnpointCooldownTime = 1f;
			/**< how long before a spawn point can be used again */ 
		public Vector2 poolLocation;
		private float nextSpawnTime; 

		// Reference Variables
		public SpawnPointScript[] spawnPoints = new SpawnPointScript[1]; 
		public Spawnable[] spawnables = new Spawnable[1];

	// -----------------------------------------------------------------------------
	// Unity Events

		// [[ ----- START ----- ]]
		void Start () {
			
			// init spawnable scripts
			for( int i = 0; i < spawnables.Length; i++ ){
				spawnables[i].id = i;
				spawnables[i].poolLocation = poolLocation; 
				spawnables[i].SetPooled();
			}

			// init spawn points
			for( int j = 0; j < spawnPoints.Length; j++ ){
				spawnPoints[j].id = j; 
				spawnPoints[j].isReady = true;
				if( !spawnPoints[j].customCooldown ){
					spawnPoints[j].cooldownTime = SpawnpointCooldownTime; 
				}
			}

			// init auto spawning
			nextSpawnTime = Time.time + autoDelay;  

		}
		
		// [[ ----- UPDATE ----- ]]
		void Update () {

			// automatic spawn
			if( automatic && Time.time > nextSpawnTime ){
				nextSpawnTime = Time.time + autoDelay; 
				SpawnRandom(); 
			}
		}

	// -----------------------------------------------------------------------------
	// Private Methods
		
		// [[ ----- SPAWNABLES AVAILABLE ----- ]]
		private bool SpawnablesAvailable(){
			for( int i = 0; i < spawnables.Length; i++ ){
				if( spawnables[i].isPooled ){ return true; }
			}
			return false; 
		}

		// [[ ----- POINTS AVAILABLE ----- ]]
		private bool PointsAvailable(){
			for( int i = 0; i < spawnPoints.Length; i++ ){
				if( spawnPoints[i].isReady ){ return true; }
			}
			return false;
		}

		// [[ ----- GET RANDOM ID ----- ]]
		// Don't use without first checking SpawnablesAvailable() !!
		private int GetRandomID(){
			int tempId = 0; 
			bool isPooled = false;
			
			while( !isPooled ){
				tempId = Random.Range( 0, spawnables.Length ); 
				isPooled = spawnables[tempId].isPooled; 
			}

			return tempId; 
		}

		// [[ ----- GET RANDOM SPAWN POINT ----- ]]
		// Don't use without first checking PointsAvailable() !!
		private int GetRandomSpawnPoint(){
			int tempId = 0; 
			bool isAvailable = false;
			
			while( !isAvailable ){
				tempId = Random.Range( 0, spawnPoints.Length ); 
				isAvailable = spawnPoints[tempId].isReady; 
			}

			return tempId; 
		}

		// [[ ----- SPAWN ----- ]]
		private void Spawn( int _spawnPoint, int _id ){
			if( spawnables[_id].isPooled 
				&& spawnPoints[_spawnPoint].isReady ){

				Vector2 spawnPos = spawnPoints[_spawnPoint].transform.position; 
				spawnPoints[_spawnPoint].SetSpawning();  
				spawnables[_id].SetAlive( spawnPos );

				if( debugOn ){ 
					Debug.Log("Spawned element: " + _id + " at " + spawnPos);
				} 
			}
		}

	// -----------------------------------------------------------------------------
	// Public Methods
		/**@brief spawn random pooled object at random ready spawn point */ 
		// [[ ----- SPAWN RANDOM ----- ]]
		public void SpawnRandom(){
			if( SpawnablesAvailable() && PointsAvailable() ){

				int id = GetRandomID();
				int sp = GetRandomSpawnPoint(); 
				Spawn( sp, id ); 

			} else if( debugOn ){Debug.LogWarning("Could not SpawnRandom !!");}
		}
		/**@brief spawn specific pooled object at random spawn point */ 
		// [[ ----- SPAWN ID AT RANDOM ----- ]]
		public void SpawnIDAtRandom( int _id ){
			if( spawnables[_id].isPooled && PointsAvailable() ){
				Spawn( GetRandomSpawnPoint(), _id );
			} else if( debugOn ){ Debug.LogWarning("Could not SpawnIDAtRandom !!");}
		}
		/**@brief spawn random pooled object at specific spawn point */ 
		// [[ ----- SPAWN AT ----- ]]
		public void SpawnAt( int _sp ){
			if( spawnPoints[_sp].isReady && SpawnablesAvailable() ){
				Spawn( _sp, GetRandomID() ); 
			} else if( debugOn ){ Debug.LogWarning("Could not SpawnAt !!");}
		}
	}
}