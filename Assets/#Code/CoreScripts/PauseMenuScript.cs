﻿using UnityEngine;
using UnityEngine.UI; 
using UnityEngine.SceneManagement;
using System.Collections;

namespace U2DProjectCore { 

	/**
	@brief Activates Pause Menu and handles logic 

	Part of Project Core Unity package.
	A component of PauseMenu_Panel - the UI object that appears during in-game 
	scenes when the pause input is pressed. References TimeController.cs to un-
	pause game.

	_Class output:_
	<br> Buttons on PauseMenu_Panel may trigger TimeController methods and
	scene-change methods

	_Class input:_
	<br> Pause menu activation is called by the OnPause event in TimeController
	<br> ToMainMenu() is called by a button on the pause menu GO

	@author Jonny Aiello
	@version 0.1.1
	@date 3/28/16
	*/ 
	public class PauseMenuScript : MonoBehaviour {

		// Variables
		private bool inputLock; 

		// Reference Variables
		private TimeController tc; 

		// [[ ----- AWAKE ----- ]]
		private void Awake(){
			// add events
			TimeController.OnTimePaused += OnPause; 
			TimeController.OnTimeNormal += OnUnpause; 

			// set panel invisible
			OnUnpause();
		}

		// [[ ----- ON DESTROY ----- ]]
		private void OnDestroy(){
			// remove events
			TimeController.OnTimePaused -= OnPause; 
			TimeController.OnTimeNormal -= OnUnpause; 
		}

		// [[ ----- START ----- ]]
		void Start () {
			if( GameObject.Find("[GameMaster]").GetComponent<TimeController>() ){
				tc = GameObject.Find("[GameMaster]").GetComponent<TimeController>();
			} else{ Debug.LogWarning("Could not find TimeController component!" ); }
		}
		
		// Update is called once per frame
		void Update () {
		
		}

	// -----------------------------------------------------------------------------
	// Private Methods

		// [[ ----- ON PAUSE ----- ]]
		private void OnPause(){
			gameObject.SetActive(true); 
		}

		// [[ ----- ON UNPAUSE ----- ]]
		private void OnUnpause(){
			gameObject.SetActive(false); 
		}

	// -----------------------------------------------------------------------------
	// Public Methods
		/**@brief called by GUI button - returns to start scene */ 
		// [[ ----- TO MAIN MENU ----- ]]
		public void ToMainMenu(){
			if( !inputLock ){
				inputLock = true;
				// save current score
				// ScoreController.SaveScoreToDB(); 
				SceneManager.LoadScene("Start");
			}
		}
		/**@brief called by GUI button - unpauses through TimeController */ 
		// [[ ----- UNPAUSE BUTTON CLICK ----- ]]
		public void UnpauseButtonClick(){
			tc.SetTimeNormal(); 
		}
	}
}