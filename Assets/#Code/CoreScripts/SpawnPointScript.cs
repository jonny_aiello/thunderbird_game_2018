﻿using UnityEngine;
using System.Collections;

namespace U2DProjectCore { 
	/**
	@brief Makes GameObject's location usable as a spawn point  

	Part of Project Core unity package.
	Attaching this script to a gameObject allows it to be included in a 
	SpawnController array, which manages the allocation of spawned objects to one
	of the available "Spawn Point" positions. Cooldown time determines the shortest
	delay between two objects being spawend at this location. coolDownTime is set to
	a default by SpawnController - can be edited in the SpawnController inspector. 
	To change the individual cooldown time of a spawn point object, check 
	"customCooldown" bool in inspector and enter the desired delay into 
	"cooldownTime." 

	_Class output:_
	<br> none

	_Class input:_
	<br> SetSpawning() is called by SpawnController() at the point of a spawn

	@author Jonny Aiello
	@version 0.1.1
	@date 3/29/16
	*/ 
	public class SpawnPointScript : MonoBehaviour {

		// Variables
		public bool customCooldown; 
		public float cooldownTime;
		[HideInInspector] public int id;
		[HideInInspector] public bool isReady;
		private float cooldownFinishedTime;
		
		// [[ ----- UPDATE ----- ]]
		void Update () {
			
			// check cooldown
			if( !isReady && (Time.time > cooldownFinishedTime) ){
				isReady = true; 
			}
		}
		/**@brief if there is coolDownTime, makes spawn unavailable until cooled */ 
		// [[ ----- SET SPAWNING ----- ]]
		public void SetSpawning(){
			isReady = false;
			cooldownFinishedTime = Time.time + cooldownTime;
		}
	}
}