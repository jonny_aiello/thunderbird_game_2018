﻿using UnityEngine;
using System.Collections;

namespace U2DProjectCore { 
	/**
	@brief Makes GameObject usable as a spawn object with SpawnController 

	Part of Project Core unity package.
	Attaching this script to a game object allows it to be addressed by 
	SpawnController, which manages the pooling, spawning, and re-use of an array
	of "Spawnable" objects. SetAlive() is called by SpawnController when spawning,
	and SetPooled() should be called by whatever script controls the lifespan of 
	this GameObject.

	_Class output:_
	<br> none

	_Class input:_
	<br> SpawnController uses SetAlive() when spawning
	<br> Scripts "destroying" this object use SetPooled() to return it to the pool

	@author Jonny Aiello
	@version 0.1.1
	@date 3/29/16
	*/ 
	public class Spawnable : MonoBehaviour {

		// Variables
		[HideInInspector] public int id;
		[HideInInspector] public bool isPooled;
		[HideInInspector] public Vector2 poolLocation;

	// -----------------------------------------------------------------------------
	// Public Methods
		/**@brief removes object from spawn pool */ 
		// [[ ----- SET ALIVE ----- ]]
		public void SetAlive( Vector2 _pos ){
			transform.position = _pos;
			isPooled = false;
			gameObject.SetActive( true ); 
		}
		/**@brief returns object to spawn pool */ 
		// [[ ----- SET POOLED ----- ]]
		public void SetPooled(){
			transform.position = poolLocation; 
			isPooled = true;
			gameObject.SetActive( false ); 
		}
	}
}