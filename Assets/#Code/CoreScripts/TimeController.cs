﻿using UnityEngine;
using System.Collections;

namespace U2DProjectCore { 
	/**
	@brief Manages flow of time and in-game pause logic 

	Part of Project Core unity package.
	Manages the passage of time in-game, contains public methods for pausing game
	action, as well as independent timers used for processing movement during a 
	pause state. Contains OnTimePaused and OnTimeNormal static events. Scripts may 
	subscribe to these if they execute logic when pausing and unpausing the game. 

	_Component of:_ 
	<br> [GameMaster] GameObject

	_Class output:_
	<br> any script subscribed to TimeController events, triggered by pause/unpause

	_Class input:_
	<br> any script which activates/deactivates a pause state or affects game speed. 

	@author Jonny Aiello
	@version 0.1.1
	@date 3/23/16
	*/ 
	public class TimeController : MonoBehaviour {

		// Static Variables
		public static float deltaTime;
		public static float lastFrameTime; 
		public delegate void PauseLogic();
		public delegate void ActiveLogic();
		public delegate void SlowmoLogic();
		public static event PauseLogic OnTimePaused;
		public static event ActiveLogic OnTimeNormal; 
		public static event SlowmoLogic OnTimeSlow; 

		// Variables


	// -----------------------------------------------------------------------------
	// Unity Events

		// [[ ----- START ----- ]]
		void Start () {
			// get real-time delta
			lastFrameTime = Time.realtimeSinceStartup; 
		}
		
		// [[ ----- UPDATE ----- ]]
		void Update () {

			// get real-time delta
			deltaTime = Time.realtimeSinceStartup - lastFrameTime; 
			lastFrameTime = Time.realtimeSinceStartup; 
		}

	// -----------------------------------------------------------------------------
	// Private Methods


	// -----------------------------------------------------------------------------
	// Public Methods
		/**@brief sets Time.timescale = 1 */ 
		// [[ ----- SET TIME NORMAL ----- ]]
		public void SetTimeNormal(){
			if( Time.timeScale != 1 ){ 
				Time.timeScale = 1;
				/*Debug.Log("TimeController: time normal");*/
				// trigger event
				if( OnTimeNormal != null ){ OnTimeNormal(); } 
			}
		}
		/**@brief sets Time.timescale = 0 */ 
		// [[ ----- SET TIME PAUSED ----- ]]
		public void SetTimePaused(){
			if( Time.timeScale != 0 ){
				Time.timeScale = 0;
				/*Debug.Log("TimeController: time paused");*/
				// trigger event
				if( OnTimePaused != null ){ OnTimePaused(); }
			}
		}
		/**@brief sets Time.timescale = 0.2 */ 
		// [[ ----- SET TIME SLOW ----- ]]
		public void SetTimeSlow(){
			if( Time.timeScale != 0.2f ){
				Time.timeScale = 0.2f; 
				/*Debug.Log("TimeController: time slowed");*/
				// trigger event
				if( OnTimeSlow != null ){ OnTimeSlow(); }
			}
		}
	}
}