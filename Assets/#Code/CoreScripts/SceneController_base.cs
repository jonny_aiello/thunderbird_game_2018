﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

namespace U2DProjectCore { 

/**
@brief Manages timing of scene-based logic 

Part of Project Core unity package.
This script can serve a number of functions related to scene-local logic and 
timing. For example, this is where you would place code needed to initialize
or synchronize other scripts at the level's start.  This is also a good location
to determine the conditions for loading the next level. 

@author Jonny Aiello
@version 0.1.1
@date 3/29/16
*/ 
public class SceneController_base : MonoBehaviour {

	// Variables
	private TimeController timeController; 

	// Reference Variables
	private GameObject gameMaster;

// -----------------------------------------------------------------------------
// Unity Events

	// [[ ----- START ----- ]]
	void Start () {

		// init references
		if( GameObject.Find("[GameMaster]") ){
			gameMaster = GameObject.Find("[GameMaster]"); 
		} else{ Debug.LogWarning("Could not find GameMaster obj!" ); }
		if( gameMaster.GetComponent<TimeController>() ){
			timeController = gameMaster.GetComponent<TimeController>(); 
		} else{ Debug.LogWarning("Could not find TimeController component!" ); }

		// init scene
		if( Time.timeScale != 1 ){ timeController.SetTimeNormal(); }
		if( ScoreController.GetScore("LvlScore") != 0 ){
			ScoreController.SetScore("LvlScore", 0, "SceneController"); 
		}

		// load score logic - only include in scenes that read/write score
		// ScoreController.LoadHighScoreFromDB(); 

	}
	
	// [[ ----- UPDATE ----- ]]
	void Update () {
	
	}

// -----------------------------------------------------------------------------
// Public Methods

	// [[ ----- ON PC DEATH ----- ]]
	public void OnPCDeath(){
		SceneManager.LoadScene("End");
	}
	
}
}