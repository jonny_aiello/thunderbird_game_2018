﻿using UnityEngine;
using System.Collections;

namespace U2DProjectCore { 

	/**
	@brief Automatically adjusts camera position to maintain pixel perfect sprites
	at multiple scales 

	Usage Directions - if sprites are imported at a pixel-per-unit other than 100, 
	change update pixelsToUnits to reflect this. In the editor, attach this script
	to the scene's main camera game object. Once you have set up your scene, enter
	the current width/height of the screen into Native Resolution. Then check the
	"Auto Scale" boolean. On awake, the script will adjust the camera size to render
	your sprites cleanly regardless of screen width/height.  Note: does not affect
	screen's width/height ratio - building a scene at 16:9 and displaying at 4:3 
	will still lead to cropping. 

	@author Jesse Freeman
	@email freej@amazon.com
	@version 0.1.1
	@date 6/2/14
	*/ 
	public class PixelPerfectOrthographic : MonoBehaviour {

		public static float pixelsToUnits = 100f; // Pixel to units
		public static float scale = 1; // Scale Value

		public bool autoScale = false;
		public Vector2 nativeResolution = new Vector2(800, 480); // Ideal resolution
		
		void Awake () {
			if (GetComponent<Camera>().orthographic) {

				if(autoScale){
					//Calculate Scale
					scale = Screen.height/nativeResolution.y;
					// print("Scale "+scale);

					//Modify pixels to units based on scale
					pixelsToUnits *= scale;
				}

				// Set camera size based on pixelToUnits
				GetComponent<Camera>().orthographicSize = (
					(Screen.height / 2.0f) / pixelsToUnits
					);
			}
		}
	}
}