﻿using UnityEngine;
using UnityEngine.UI; 
using System.Collections;

namespace U2DProjectCore { 
	/**
	@brief Controls on-screen representation of a game score 

	Part of Project Core unity package.
	This script is intended to be a component of a GUI object and holds a reference
	to a public GUI Text object, which it will update to show a current score. It
	requires an instance of the static class ScoreController to be present in the 
	project to function. Each score tracked in ScoreController has a string label - 
	every ScoreGuiScript GameObject must have a "scoreToDisplay" value matching one 
	of these labels. The score is then automatically updated using 
	ScoreController.OnScoreChanged event every time an internal score has been 
	changed. 

	_Component of:_ 
	<br> Timer_Panel GameObject

	_Class output:_
	<br> Updates the text of a GUI Text component

	_Class input:_
	<br> Triggered by an event on ScoreController

	@author Jonny Aiello
	@version 0.1.1
	@date 3/30/16
	*/ 
	public class ScoreGuiScript : MonoBehaviour {

		// Variables
		public string scoreToDisplay = "LvlScore"; 
			/**< must match the dictionary key string of one of the scores found in
			ScoreController.scores */
		public string prefixText = "Score: "; 
		public string scoreDigits = "000000"; 

		// Reference Variables
		public Text scoreText; 

	// -----------------------------------------------------------------------------
	// Unity Events
		
		// [[ ----- ON ENABLE ----- ]]
		private void OnEnable(){
			ScoreController.OnScoreChanged += UpdateText;
		}

		// [[ ----- ON DISABLE ----- ]]
		private void OnDisable(){
			ScoreController.OnScoreChanged -= UpdateText; 
		}

		// [[ ----- START ----- ]]
		void Start () {
			UpdateText(); 
		}

	// -----------------------------------------------------------------------------
	// Private Methods

		// [[ ----- UPDATE TEXT ----- ]]
		private void UpdateText(){
			int intText = ScoreController.GetScore( scoreToDisplay );
			scoreText.text = prefixText + intText.ToString( scoreDigits ); 
		}
	}
}