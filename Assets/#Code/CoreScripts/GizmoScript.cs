﻿using UnityEngine;
using System.Collections;

namespace U2DProjectCore { 
	/**
	@brief Draws shapes in-editor for visual debugging purposes 

	Part of Project Core unity package.
	Attaching this script to a gameObject will draw a "gizmo" shape at the location
	of that game object in-editor. The script supports 3 kinds of gizmos - squares,
	circles and lines. Gizmo type can be selected by a drop-down menu in the 
	inspector, and dimensions of the gizmo can also be set in the inspector fields
	or changed programatically during runtime by other scripts. 

	_Class output:_
	<br> none

	_Class input:_
	<br> Any script that wants to represent logic visually through attached gizmo

	@author Jonny Aiello
	@version 0.1.1
	@date 3/30/16
	*/ 
	public class GizmoScript : MonoBehaviour {

		// Variables
		public Color color = new Color(0.985f, 0.022f, 0.022f, 0.2f);
		public Shape shape = Shape.SQUARE;
		public float circleRadius = 0.5f;
		public Vector3 squareSize = new Vector3(1, 1, 0);
		public Vector3 lineEndpoint = new Vector3(0, 0, 0); 

		// [[ ----- SHAPE ENUM ----- ]]
		public enum Shape{
			SQUARE,
			CIRCLE,
			LINE
		}

		// [[ ----- ON DRAW GIZMOS ----- ]]
		void OnDrawGizmos() {
			Gizmos.color = color;

			switch( shape ){
				case Shape.SQUARE:
					Gizmos.DrawCube( transform.position, squareSize );
					break;
				case Shape.CIRCLE:
					Gizmos.DrawSphere(transform.position, circleRadius);
					break;
				case Shape.LINE:
					Gizmos.DrawLine(transform.position, lineEndpoint);
					break;
				default:
					break;
			}
		}
	}
}