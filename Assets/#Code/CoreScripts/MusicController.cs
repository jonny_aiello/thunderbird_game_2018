﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace U2DProjectCore { 
	/**
	@brief Manages the music tracks playing during the game 

	Part of Project Core unity package.
	Works as part of [GameMaster] game object - music tracks are added as childed
	game objects containing audio clips. Contains public methods for controlling
	currently playing track, switching track, and modifying volume. 

	_Component of:_ 
	<br> [GameMaster] GameObject

	_Class output:_
	<br> none

	_Class input:_
	<br> Any script that needs to access or modify game music

	@author Jonny Aiello
	@version 0.1.1
	@date 3/23/16
	*/ 

	public class MusicController : MonoBehaviour {

		// Variables
		public bool musicOn;
		public int startingTrackNo;
		public float duckRatio = 0.25f; 
		private float defaultVolume;
		private float lastVolume;

		// Transition variables
		/*public bool transitionsOn;
		public float transitionSeconds = 1f;
		private bool transitioning;
		private float startVol;
		private float endVol;
		private float curTransVol;
		private float timer = 0f;
		private float speedMod = 0.1f;
		private float fps60 = 0.017f; 
		private float progress = 0f;*/

		// Reference Variables
		public AudioClip[] trackList = new AudioClip[0];
		private AudioSource audioSource;

		// Properties
		/*public bool TransitionsOn { 
			get{return transitionsOn;} 
			set{transitionsOn = value;} 
		}*/


	// -----------------------------------------------------------------------------
	// Unity Events

		// [[ ----- START ----- ]]
		void Start () {
			// init
			audioSource = GetComponent<AudioSource>(); 
			defaultVolume = audioSource.volume;
			lastVolume = audioSource.volume;
			
			// play track
			if( musicOn ){ 
				SetTrack(startingTrackNo);
				PlayTrack();
			}

		}

		// [[ ----- UPDATE ----- ]]
		void Update(){
			/*if( transitioning ){ audioSource.volume = curTransVol; }*/
		}

	// -----------------------------------------------------------------------------
	// Private Methods

		// [[ ----- TRANSITION ----- ]]
		/*IEnumerator Transition () {

			// start transition
			transitioning = true;
			// fade in - turn music on
			if( endVol != 0 ){
				audioSource.volume = 0;
				PlayTrack();  
			}

			while( timer <= transitionSeconds ) {
				timer += speedMod;  
				progress = timer / transitionSeconds;
				curTransVol = Mathf.Lerp( startVol, endVol, progress );
				
				yield return new WaitForSeconds( fps60 );
			}
			
			// reset
			timer = 0f;
			progress = 0f;

			// end transition
			transitioning = false; 
			// fade out - turn music off
			if( endVol == 0 ){ StopTrack(); }
			else{ audioSource.volume = endVol; }
		}*/


	// -----------------------------------------------------------------------------
	// Public Methods
		/**@brief load track to play */ 
		// [[ ----- SET TRACK ----- ]]
		public void SetTrack( int _num ){
			if( _num < trackList.Length ){ 
				if( trackList[_num] ){ audioSource.clip = trackList[_num]; }
				else{ Debug.LogWarning("MC: track not found! " + _num); }
			}else{
				Debug.LogWarning("MC: track not found! " + _num);
				 
			}
		}
		/**@brief play/resume currently loaded track */ 
		// [[ ----- PLAY TRACK ----- ]]
		public void PlayTrack(){
			/*// fade in track
			if( transitionsOn ){
				startVol = 0;
				endVol = lastVolume; 
				StartCoroutine("Transition"); 
			// play instant
			}else{ audioSource.Play(); }*/
			if( audioSource.clip != null ){ audioSource.Play(); }
		}
		/**@brief pause loaded track */ 
		// [[ ----- PAUSE TRACK ----- ]]
		public void PauseTrack(){
			audioSource.Pause();
		}
		/**@brief stop loaded track */ 
		// [[ ----- STOP TRACK ----- ]]
		public void StopTrack(){
			audioSource.Stop(); 
		}
		/**@brief set or unset volume by a percentage - duckRatio */ 
		// [[ ----- DUCK TRACK ----- ]]
		public void DuckTrack( bool _isEnabled ){
			if( _isEnabled ){ 
				lastVolume =  audioSource.volume;
				audioSource.volume = audioSource.volume * duckRatio; 
			}
			else{ audioSource.volume = lastVolume; }
		}
		/**@brief set volume */ 
		// [[ ----- SET TRACK VOLUME ----- ]]
		public void SetTrackVolume( float _vol ){
			lastVolume = audioSource.volume; 
			audioSource.volume = _vol; 
		}
		/**@brief return volume to starting level */ 
		// [[ ----- RESET TRACK VOLUME ----- ]]
		public void ResetTrackVolume(){
			audioSource.volume = defaultVolume; 
		}
		/**@brief enable/disable loop boolean for track */ 
		// [[ ----- SET TRACK LOOP ----- ]]
		public void SetTrackLoop( bool _isEnabled ){
			if( _isEnabled ){ audioSource.loop = true; }
			else{ audioSource.loop = false; }
		}		
	}
}