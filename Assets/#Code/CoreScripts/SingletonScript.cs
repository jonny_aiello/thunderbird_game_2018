using UnityEngine;
using System.Collections;

namespace U2DProjectCore { 
	/**
	@brief Makes attached GameObject a singleton - only one instance loads per game

	@author Jonny Aiello
	@version 0.1.1
	@date 3/23/16
	*/ 
	public class SingletonScript : MonoBehaviour {
		
		public static SingletonScript singleton;
		
		/*public float samplePersistantData1; 
		public float samplePersistantData2;*/
		
		void Awake () {
			// singleton logic
			if( singleton == null ){
				DontDestroyOnLoad( gameObject );
				singleton = this; 
			} else if( singleton != this ){
				Destroy( gameObject ); 
			}
		} 
	}
}