﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using U2DPlatformer; 

public class SceneController_tutorial : MonoBehaviour {

	// enum
	private enum State{
		Shooting,
		Moving,
		Dashing,
		AimLocking,
		AirBraking,
		Done,
		Transition
	}

	// Variables
	private State state;
	private float holdTime = 3f; 
	private float elapsedTime; 
	private bool stateChange; 
	private bool timeSlip;

	public Color completeColor;
	private Color baseColor; 

	// Reference Variables
	public Text topText;
	public Text bottomText;
	public Image bottomPanel; 

	// Use this for initialization
	void Start () {
		state = State.Shooting;
		stateChange = true; 
		baseColor = bottomPanel.GetComponent<Image>().color; 
	}
	
	// Update is called once per frame
	void Update () {
		
		
		switch( state ){
			
			case State.Shooting:
				if( stateChange ){
					stateChange = false; 
					timeSlip = true; 
					holdTime = 3f; 
					topText.text = "HOLD THE \"F\" KEY TO SHOOT"; 
					bottomText.text = "Try shooting for 3 seconds"; 
				}
				ProcessTimer( PCS.M.Shoot, State.Moving ); 
				break;
			
			case State.Moving:
				if( stateChange ){
					stateChange = false;
					timeSlip = true; 
					holdTime = 5f; 
					topText.text = "USE THE ARROW KEYS TO MOVE"; 
					bottomText.text = "Try moving for 5 seconds"; 
				}
				ProcessTimer( PCS.M.Thrust, State.Dashing ); 
				break;
			
			case State.Dashing:
				if( stateChange ){
					stateChange = false;
					timeSlip = false; 
					holdTime = .9f;  
					topText.text = "PRESS THE \"SPACE\" KEY TO DASH"; 
					bottomText.text = "Try Dashing 3 times"; 
				}
				ProcessTimer( PCS.M.Dash, State.AimLocking ); 
				break;
			
			case State.AimLocking:
				if( stateChange ){
					stateChange = false;
					timeSlip = true; 
					holdTime = 5f;  
					topText.text = "HOLD THE \"Z\" KEY TO LOCK YOUR AIM"; 
					bottomText.text = "Try moving with locked aim for 5 seconds"; 
				}
				ProcessTimer( PCS.M.AimLock, State.AirBraking ); 
				break;
			
			case State.AirBraking:
				if( stateChange ){
					stateChange = false;
					timeSlip = true; 
					holdTime = 3f;  
					topText.text = "HOLD THE \"LEFT SHIFT\" KEY TO AIR BRAKE"; 
					bottomText.text = "Try aiming while Air Braking for 3 seconds"; 
				}
				ProcessTimer( PCS.M.AirBrake, State.Done ); 
				break;
			
			case State.Done:
				if( stateChange ){
					stateChange = false;
					timeSlip = false; 
					holdTime = 0.2f;  
					topText.text = "COMPLETED!"; 
					bottomText.text = "Dash to continue"; 
				}
				ProcessTimer( PCS.M.Dash, State.Transition ); 
				break;
			
			case State.Transition:
				if( stateChange ){
					stateChange = false;
					SceneManager.LoadScene("Start");
				}
				break;

			default:
				break;
		}
	}


	// [[ ----- PROCESS TIMER ----- ]]
	private void ProcessTimer( PCS.M _moveSlot, State _state ){
		bool timerComplete = false;
		if( ModularPlayerController.reference.IsBehaviorTriggered(_moveSlot) ){ 
			timerComplete = CheckTimer(); 
		
		}else if( timeSlip ){ DecreaseTimer(); }

		if( timerComplete ){ state = _state; }
	}

	// [[ ----- CHECK TIMER ----- ]]
	private bool CheckTimer(){
		elapsedTime += Time.deltaTime;
		
		SetPanelColor();

		if( elapsedTime > holdTime ){
			Debug.Log("DING");
			elapsedTime = 0;
			stateChange = true; 
			bottomPanel.color = baseColor; 
			return true;
		}
		return false; 
	}

	// [[ ----- DECREASE TIMER ----- ]]
	private void DecreaseTimer(){
		if( elapsedTime > 0 ){ 
			elapsedTime -= Time.deltaTime; 
			SetPanelColor(); 
		}
	}

	// [[ ----- SET PANEL COLOR ----- ]]
	private void SetPanelColor(){
		// set color
		float ratio = elapsedTime / holdTime; 
		bottomPanel.color = Color.Lerp( baseColor, completeColor, ratio ); 

	}
}
