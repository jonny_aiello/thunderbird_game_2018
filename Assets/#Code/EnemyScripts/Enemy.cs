﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using U2DPlatformer;

public interface iEnemyState {

	void StateEnter();
	void StateExit();
	iEnemyState Think();
	void Act();

}

public abstract class Enemy : TopDownUnit {

	protected iEnemyState state;
	protected bool isAlive;
	protected Collider2D lastCollision; 

	// [[ ----- UPDATE ----- ]]	
	void Update () {
		if( isAlive ){
			iEnemyState newState = state.Think();
			if( newState != null ){ 
				state.StateExit();
				state = newState; 
				state.StateEnter();
			}
			state.Act();
		}
	}

	// [[ ----- ON TRIGGER ENTER 2D ----- ]]
	private void OnTriggerEnter2D( Collider2D _c ){
		lastCollision = _c;
	}

	// [[ ----- GET LAST COLLISION ----- ]]
	public Collider2D GetLastCollision(){
		Collider2D lc = lastCollision;
		lastCollision = null; 
		return lc; 
	}
}
