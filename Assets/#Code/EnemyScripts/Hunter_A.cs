﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using U2DProjectCore;

[RequireComponent(typeof(Spawnable))]
public class Hunter_A : Enemy {

	// Variables
	// private Color baseColor = Color.white; 
	// private Color hurtColor = Color.red; 
	public bool debug; 
	public float moveSpeed = 1; 
	public float turnSpeed = 5; 
	public float raycastDist = 1f; 
	public float windupTime = 0.5f; 
	public float headbuttDelayTime = 0.25f; 
	public float headbuttForce = 1f; 
	public float dashDuration = 1f; 
	public float headbuttDrag = 0.5f;
	public LayerMask playerLayer; 

	// Reference Variables
	private Transform player;
	private Spawnable spawnable;
	[HideInInspector] public Rigidbody2D rigidbody;  
	 

// -----------------------------------------------------------------------------
// Unity Events

	// [[ ----- AWAKE ----- ]]
	private void Awake(){
		maxHP = 5; 
		pointAtSpeed = turnSpeed; 
	}

	// [[ ----- START ----- ]]
	void Start () {
		player = GameObject.FindWithTag("Player").transform;
		sprite = transform.GetChild(0); 
		spawnable = GetComponent<Spawnable>(); 
		rigidbody = GetComponent<Rigidbody2D>(); 
		spawnable.SetPooled();
	}

	// [[ ----- ON ENABLE ----- ]]
	private void OnEnable(){
		OnSpawn();
	}
	

// -----------------------------------------------------------------------------
// TopDownUnit Methods

	// [[ ----- ON SPAWN ----- ]]
	public override void OnSpawn(){
		isAlive = true; 
		currentHP = maxHP;
		state = NewFollowState();
		sprite.GetComponent<SpriteRenderer>().color = baseColor; 

	}

	// [[ ----- ON DEATH ----- ]]
	public override void OnDeath(){
		isAlive = false; 
		spawnable.SetPooled();
	}

// -----------------------------------------------------------------------------
// Get States
	
	// [[ ----- NEW FOLLOW STATE ----- ]]
	public HunterState_Follow NewFollowState(){
		return new HunterState_Follow( 
			this,
			player, 
			moveSpeed, 
			raycastDist );
	}

	// [[ ----- NEW AIM STATE ----- ]]
	public HunterState_Aim NewAimState(){
		return new HunterState_Aim( this, player, windupTime ); 
	}

	// [[ ----- NEW DASH ATTACK STATE ----- ]]
	public HunterState_DashAttack NewDashAttackState(){
		return new HunterState_DashAttack(
			this,
			player,
			headbuttDelayTime,
			headbuttForce,
			dashDuration,
			headbuttDrag );
	}
	
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

public class HunterState_Follow : iEnemyState {

	// Variables
	private Hunter_A enemy; 
	private Transform player; 
	private float moveSpeed; 
	private float raycastDist; 
	private int raycastIntCounter; 

	// [[ ----- CONSTRUCTOR ----- ]]
	public HunterState_Follow(
		Hunter_A _enemy,
		Transform _player, 
		float _moveSpeed,
		float _raycastDist ){

		enemy = _enemy;
		player = _player; 
		moveSpeed = _moveSpeed; 
		raycastDist = _raycastDist; 
	}

	// [[ ----- STATE ENTER ----- ]]
	public void StateEnter(){

	}

	// [[ ----- STATE EXIT ----- ]]
	public void StateExit(){

	}

	// [[ ----- THINK ----- ]]
	public iEnemyState Think(){

		// check collisions
		Collider2D lastCollision = enemy.GetLastCollision();
		if( lastCollision != null ){
			// player
			if( lastCollision.gameObject.layer == 8 ){ enemy.OnDeath(); }
			// Bullet_player
			if( lastCollision.gameObject.layer == 10 ){ enemy.UpdateHP( -1 ); }
		}

		// check range for headbutt
		raycastIntCounter++;
		if( raycastIntCounter > 9 ){
			raycastIntCounter = 0; 

			// draw debug line
			if( enemy.debug ){
				Vector2 raycast = enemy.ForwardVector() * raycastDist; 
				Vector2 endPoint = (Vector2)enemy.transform.position + raycast; 
				Debug.DrawLine( enemy.transform.position, endPoint, Color.white, 0.1f ); 
			}
			
			// Raycast
			RaycastHit2D hit = Physics2D.Raycast( 
				enemy.transform.position, 
				enemy.ForwardVector(),
				raycastDist,
				enemy.playerLayer
				);
			if( hit.collider != null ){
				return enemy.NewAimState();
			}
		}


		return null;
	}

	// [[ ----- ACT ----- ]]
	public void Act(){

		enemy.PointAt( player );  
		Vector2 dirToTarget = player.position - enemy.transform.position;
		dirToTarget.Normalize();
		Vector2 moveVector = dirToTarget * (moveSpeed * Time.deltaTime); 
		enemy.transform.Translate( moveVector ); 
	}
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

public class HunterState_Aim : iEnemyState {

	// Variables
	private Hunter_A enemy;
	private Transform player; 
	private float aimTime; 
	private float elapsedTime; 

	// [[ ----- CONSTRUCTOR ----- ]]
	public HunterState_Aim( Hunter_A _enemy, Transform _player, float _windupTime ){
		enemy = _enemy;
		player = _player; 
		aimTime = _windupTime; 
	}

	// [[ ----- STATE ENTER ----- ]]
	public void StateEnter(){

	}

	// [[ ----- STATE EXIT ----- ]]
	public void StateExit(){

	}

	// [[ ----- THINK ----- ]]
	public iEnemyState Think(){

		// check collisions
		Collider2D lastCollision = enemy.GetLastCollision();
		if( lastCollision != null ){
			// player
			if( lastCollision.gameObject.layer == 8 ){ enemy.OnDeath(); }
			// Bullet_player
			if( lastCollision.gameObject.layer == 10 ){ enemy.UpdateHP( -1 ); }
		}

		// Timer
		elapsedTime += Time.deltaTime;
		if( elapsedTime > aimTime ){
			return enemy.NewDashAttackState();
		}

		return null;
	}

	// [[ ----- ACT ----- ]]
	public void Act(){
		enemy.PointAt( player ); 
	}
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

public class HunterState_DashAttack : iEnemyState {

	// Variables
	private Hunter_A enemy; 
	private Transform player; 
	private float delayTime;
	private float dashForce; 
	private float linearDrag; 
	private float dashTime; 

	private float delayElapsedTime;
	private float dashElapsedTime;
	private bool delayElapsed; 
	private bool dashInitiated; 

	// [[ ----- CONSTRUCTOR ----- ]]
	public HunterState_DashAttack( 
		Hunter_A _enemy,
		Transform _player,
		float _delayTime,
		float _dashForce,
		float _dashTime,
		float _linearDrag ){

		enemy = _enemy;
		player = _player; 
		delayTime = _delayTime;
		dashForce = _dashForce;
		dashTime = _dashTime; 
		linearDrag = _linearDrag; 
	}

	// [[ ----- STATE ENTER ----- ]]
	public void StateEnter(){
		enemy.rigidbody.bodyType = RigidbodyType2D.Dynamic;
		enemy.rigidbody.gravityScale = 0; 
		enemy.rigidbody.drag = linearDrag;
	}

	// [[ ----- STATE EXIT ----- ]]
	public void StateExit(){
		enemy.rigidbody.velocity = Vector2.zero;
		enemy.rigidbody.bodyType = RigidbodyType2D.Kinematic; 
	}

	// [[ ----- THINK ----- ]]
	public iEnemyState Think(){

		// check collisions
		Collider2D lastCollision = enemy.GetLastCollision();
		if( lastCollision != null ){
			// player
			if( lastCollision.gameObject.layer == 8 ){ enemy.OnDeath(); }
			// Bullet_player
			if( lastCollision.gameObject.layer == 10 ){ enemy.UpdateHP( -1 ); }
		}

		// delay timer
		if( !delayElapsed ){
			delayElapsedTime += Time.deltaTime;
			if( delayElapsedTime > delayTime ){
				delayElapsed = true; 
			}
		}

		// dash timer
		if( delayElapsed && dashInitiated ){
			dashElapsedTime += Time.deltaTime;
			if( dashElapsedTime > dashTime ){
				return enemy.NewFollowState();
			}
		}

		return null;
	}

	// [[ ----- ACT ----- ]]
	public void Act(){
		if( enemy.debug ){
			 
		}

		if( delayElapsed && !dashInitiated ){
			dashInitiated = true; 
			enemy.rigidbody.velocity = enemy.ForwardVector() * dashForce; 
		}
	}
}
