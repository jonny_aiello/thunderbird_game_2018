﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace U2DPlatformer {

public abstract class TopDownUnit : MonoBehaviour {

	// Variables
	protected int maxHP;
	protected int currentHP;
	protected float pointAtSpeed; 
	public abstract void OnSpawn();
	public abstract void OnDeath();

	// Temp Color-shift variables
	protected Color baseColor = Color.white; 
	protected Color hurtColor = Color.red; 
	public Transform sprite; 
		// reference to sprite must be established in the inheriting class on init

	// [[ ----- UPDATE HP ----- ]]
	public void UpdateHP( int _value ){
		currentHP += _value;
		Mathf.Clamp(currentHP, 0, maxHP ); 

		// temp color-change logic
		float hpRemainingRatio = (float)currentHP / (float)maxHP; 
		Color newColor = Color.Lerp( hurtColor, baseColor, hpRemainingRatio );
		sprite.GetComponent<SpriteRenderer>().color = newColor; 

		// kill
		if( currentHP == 0 ){ OnDeath(); }
	}

	// [[ ----- FORWARD VECTOR ----- ]]
	public Vector2 ForwardVector(){
		float radians = sprite.eulerAngles.z * Mathf.Deg2Rad;
		Vector2 forwardVect = new Vector2(Mathf.Cos(radians), Mathf.Sin(radians));
		forwardVect.Normalize();
		return forwardVect;  
	}

	// [[ ----- POINT AT ----- ]]
	public void PointAt( Transform _target ){
		Vector3 vectorToTarget = _target.position - sprite.position;
		float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
		Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
		sprite.rotation = Quaternion.Slerp(
			sprite.rotation, q, Time.deltaTime * pointAtSpeed); 
	}
}
}
