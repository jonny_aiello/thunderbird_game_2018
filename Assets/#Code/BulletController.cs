﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace U2DPlatformer {

public class BulletController : MonoBehaviour {

	// Variables
	public static BulletController reference; 
	public static List<Bullet> pcBullets = new List<Bullet>(); 

	// [[ ----- AWAKE ----- ]]
	private void Awake(){
		reference = this; 
	}

	// [[ ----- ON DISABLE ----- ]]
	private void OnDisable(){
		pcBullets.Clear();
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// [[ ----- GET BULLET ----- ]]
	public Bullet GetBullet(){
		Bullet availableBullet = pcBullets
			.Where( b => !b.IsActive )
			.FirstOrDefault();
		return availableBullet; 
	}
}
}