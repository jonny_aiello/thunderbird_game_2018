﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

namespace U2DPlatformer {
/**
@brief Basic structure for MPC_MoveBehavior classes 

Manages the directional facing of the sprite.

@author Jonny Aiello
@version 1.2.0
@date 5/22/17
*/ 
public class MB_AirBrake : MPC_MoveBehavior {

	//Variables
	private PCS.M myID = PCS.M.AirBrake;
	private bool isActive = false; 
	private bool moveCompleted = true;

	public float additiveDrag = 3f; 
	private bool airBrakePressed;
	private bool stateSetLock;
	
	//Requirement / Incompatables Lists
	private PCS.M[] incompatablesList = new PCS.M[1]{ 
		PCS.M.OnHit 
	}; 
	private PCS.M[] requiredsList = new PCS.M[0]{ 
		//PCS.M.Jump 
	};


	//Properties
	public override PCS.M MyID { get{return myID;} }

// -----------------------------------------------------------------------------
// MPC-Called Methods

	// [[ ----- CHECK TRIGGER ----- ]]
	public override bool CheckTrigger(){

		// Determine state + input based activation 
		if( mpcRef.GetInputDown("airBrake") ){ airBrakePressed = true; }
		if( mpcRef.GetInputUp("airBrake") ){ airBrakePressed = false; }

		// Triggers
		if( moveCompleted && airBrakePressed ){ moveCompleted = false; }
		
		
		// determine if move is in progress
		if( !moveCompleted ){ isActive = true; }
		else{ isActive = false; }

		// Check compatability with other moves
		if( isActive ){ 
			// Check incompatables list against higher-priority moves
			isActive = !PCS.CheckIncompatabilities( 
				mpcRef.GetTriggeredBehaviors(), incompatablesList, (int)myID );
			if( !isActive ){ EndMove(); }
			else{ 
				// Check requireds list against all moves
				isActive = PCS.CheckRequirements( 
					mpcRef.GetTriggeredBehaviors(), requiredsList );
				if( !isActive ){ EndMove(); }
			}
		}

		return isActive; 
	}

	// [[ ----- APPLY MOVEMENT ----- ]]
	public override void ApplyMovement( Rigidbody2D _rb2d ){

		if( !stateSetLock ){ 
			mpcRef.SetState( PCS.S.AirBraking, true );
			stateSetLock = true;
			_rb2d.drag += additiveDrag; 
		}

		if( !airBrakePressed ){ EndMove(); }
	
	}

	// [[ ----- NOMINATE ANIM ----- ]]
	public override PCS.A NominateAnim(){
		// if no animation associated w/ this move, return PCS.A.TOTAL
		
		PCS.A currAnim = PCS.A.TOTAL;
		

		return currAnim;   
	}

// -----------------------------------------------------------------------------
// Other Methods

	// [[ ----- END MOVE ----- ]]
	public override void EndMove(){
		ResetInputs();
		mpcRef.SetState( PCS.S.AirBraking, false );
		stateSetLock = false; 
		moveCompleted = true;
	}

	// [[ ----- RESET INPUTS ----- ]]
	private void ResetInputs(){
		
	}
}
}