﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

namespace U2DPlatformer {
/**
@brief Behavior logic used with ModularPlayerController class 

@author Jonny Aiello
@version 1.2.0
@date 5/22/17
*/ 
public class MB_Idle : MPC_MoveBehavior {

	//Variables
	private PCS.M myID = PCS.M.Idle;
	private bool isActive = false; 
	private bool moveCompleted = true;

	private float baseDrag;
	
	//Requirement / Incompatables Lists
	private PCS.M[] incompatablesList = new PCS.M[5]{ 
		PCS.M.OnHit,
		PCS.M.Dash,
		PCS.M.Aim,
		PCS.M.AirBrake,
		PCS.M.Thrust
	};
	/*private PCS.M[] requiredsList = new PCS.M[1]{ 
		PCS.M.Jump 
	};*/

	//Properties
	public override PCS.M MyID { get{return myID;} }

	// [[ ----- START ----- ]]
	private void Start(){
		baseDrag = mpcRef.GetComponent<Rigidbody2D>().drag; 
	}

// -----------------------------------------------------------------------------
// MPC-Call Methods

	// [[ ----- CHECK TRIGGER ----- ]]
	public override bool CheckTrigger(){
		
		// Determine state + input based activation 
		isActive = true; 

		// Check compatability with other moves
		if( isActive ){ 
			// Check incompatables list against higher-priority moves
			isActive = !PCS.CheckIncompatabilities( 
				mpcRef.GetTriggeredBehaviors(), incompatablesList, (int)myID );
			if( !isActive ){ moveCompleted = true; }
			/*else{ 
				// Check requireds list against all moves
				isActive = PCS.CheckRequirements( 
					mpcRef.GetTriggeredBehaviors(), requiredsList );
				if( !isActive ){ moveCompleted = true; }
			}*/
		}

		return isActive; 
	}

	// [[ ----- APPLY MOVEMENT ----- ]]
	public override void ApplyMovement( Rigidbody2D _rb2d ){
		if( _rb2d.drag != baseDrag ){ _rb2d.drag = baseDrag; }
	}

	// [[ ----- NOMINATE ANIM ----- ]]
	public override PCS.A NominateAnim(){
		
		PCS.A currAnim = PCS.A.Idle;
		// if( !isGrounded ){ currAnim = PCS.A.Fall; }

		return currAnim;  
	}
// -----------------------------------------------------------------------------
// Other Methods

	// [[ ----- END MOVE ----- ]]
	public override void EndMove(){
		ResetInputs();
		// moveCompleted = true;
	}

	// [[ ----- RESET INPUTS ----- ]]
	private void ResetInputs(){
		
	}
}
}