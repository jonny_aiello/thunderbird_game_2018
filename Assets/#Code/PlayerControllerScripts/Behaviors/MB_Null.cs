﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

namespace U2DPlatformer {
/**
@brief Placeholder Move Behavior

Used during testing or when a certain move behavior slot is unavailable in a 
given mode. This version of MB_Null lists its "myID" variable as a public enum,
which lets the user create multiple null scripts and set them to the appropriate
slot. 

_Component of:_
<br> none

_Class output:_
<br> none

_Class input:_
<br> none

@author Jonny Aiello
@version 1.2.0
@date 5/22/17
*/ 
public class MB_Null : MPC_MoveBehavior {

	//Variables
	public PCS.M myID;
	private bool isActive = false;

	//Properties
	public override PCS.M MyID { get{return myID;} }

	// [[ ----- CHECK TRIGGER ----- ]]
	public override bool CheckTrigger(){

		return isActive; 
	}

	// [[ ----- APPLY MOVEMENT ----- ]]
	public override void ApplyMovement( Rigidbody2D _rb2d ){

	}

	// [[ ----- NOMINATE ANIM ----- ]]
	public override PCS.A NominateAnim(){
		// if no animation associated w/ this move, return PCS.A.TOTAL
		return PCS.A.TOTAL; 
	}

// -----------------------------------------------------------------------------
// Other Methods

	// [[ ----- END MOVE ----- ]]
	public override void EndMove(){
		
	}

	// [[ ----- RESET INPUTS ----- ]]
	private void ResetInputs(){
		
	}
}
}