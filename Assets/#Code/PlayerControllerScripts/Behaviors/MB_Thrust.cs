﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

namespace U2DPlatformer {
/**
@brief Basic structure for MPC_MoveBehavior classes 

This is a template intended to be copied and edited when making a new Move 
Behavior to use with the ModularPlayerController class. Contains default code to
indicate typical usage within the MPC framework.

@author Jonny Aiello
@version 1.2.0
@date 5/22/17
*/ 
public class MB_Thrust : MPC_MoveBehavior {

	//Variables
	private PCS.M myID = PCS.M.Thrust;
	private bool isActive = false; 
	private bool moveCompleted = true;

	public float accelForce = 2.3f;
	public float accelLimit = 2.4f; 
	public float linearDrag = 0.2f; 

	private float hVal;
	private float vVal; 
	private bool inputFound;
	private bool dragDisengaged;

	private float baseDrag;
	
	//Requirement / Incompatables Lists
	private PCS.M[] incompatablesList = new PCS.M[3]{ 
		PCS.M.AirBrake,
		PCS.M.OnHit, 
		PCS.M.Dash
	}; 
	private PCS.M[] requiredsList = new PCS.M[0]{ 
		//PCS.M.Jump 
	};


	//Properties
	public override PCS.M MyID { get{return myID;} }

	// [[ ----- START ----- ]]
	private void Start(){
		baseDrag = mpcRef.GetComponent<Rigidbody2D>().drag; 
	}

// -----------------------------------------------------------------------------
// MPC-Called Methods

	// [[ ----- CHECK TRIGGER ----- ]]
	public override bool CheckTrigger(){

		// Determine state + input based activation 
		hVal = mpcRef.GetAxis("h");
		vVal = mpcRef.GetAxis("v");
		inputFound = ( hVal != 0 || vVal != 0 );

		// Triggers
		if( moveCompleted && inputFound ){
			moveCompleted = false; 
		}
		
		// determine if move is in progress
		if( !moveCompleted ){ isActive = true; }
		else{ isActive = false; }

		// Check compatability with other moves
		if( isActive ){ 
			// Check incompatables list against higher-priority moves
			isActive = !PCS.CheckIncompatabilities( 
				mpcRef.GetTriggeredBehaviors(), incompatablesList, (int)myID );
			if( !isActive ){ EndMove(); }
			else{ 
				// Check requireds list against all moves
				isActive = PCS.CheckRequirements( 
					mpcRef.GetTriggeredBehaviors(), requiredsList );
				if( !isActive ){ EndMove(); }
			}
		}

		return isActive; 
	}

	// [[ ----- APPLY MOVEMENT ----- ]]
	public override void ApplyMovement( Rigidbody2D _rb2d ){

		if( !dragDisengaged && !moveCompleted ){
			_rb2d.drag = linearDrag;
			dragDisengaged = true;
		}

		bool bothAxis = (hVal != 0 && vVal != 0); 
		float topSpeed = bothAxis ? accelLimit : accelLimit * 1.5f; 

		// calc individual axis thrust
		float hForce = 0; 
		if( Mathf.Abs(_rb2d.velocity.x) < topSpeed ){hForce = hVal * accelForce;}
		float vForce = 0; 
		if( Mathf.Abs(_rb2d.velocity.y) < topSpeed ){vForce = vVal * accelForce;}
		Vector2 flightVector = new Vector2( hForce, vForce ); 
		_rb2d.AddForce( flightVector ); 

		// end move
		if( !inputFound ){ EndMove(); }
	}

	// [[ ----- NOMINATE ANIM ----- ]]
	public override PCS.A NominateAnim(){
		// if no animation associated w/ this move, return PCS.A.TOTAL
		
		PCS.A currAnim = PCS.A.TOTAL;
		

		return currAnim;   
	}

// -----------------------------------------------------------------------------
// Other Methods

	// [[ ----- END MOVE ----- ]]
	public override void EndMove(){
		ResetInputs();
		dragDisengaged = false; 
		moveCompleted = true;
	}

	// [[ ----- RESET INPUTS ----- ]]
	private void ResetInputs(){
		
	}
}
}