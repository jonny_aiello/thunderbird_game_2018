﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

namespace U2DPlatformer {
/**
@brief Basic structure for MPC_MoveBehavior classes 

This is a template intended to be copied and edited when making a new Move 
Behavior to use with the ModularPlayerController class. Contains default code to
indicate typical usage within the MPC framework.

@author Jonny Aiello
@version 1.2.0
@date 5/22/17
*/ 
public class MB_Dash : MPC_MoveBehavior {

	// enum
	private enum DashState{
		Charged,
		Thrust,
		Drag,
		Cooldown
	}

	//Variables
	private PCS.M myID = PCS.M.Dash;
	private bool isActive = false; 
	private bool moveCompleted = true;

	private Color basicColor = Color.white;
	public Color coolColor = Color.blue;

	private bool dashPressed;
	private bool dragApplied;
	private DashState state = DashState.Charged; 

	// timer variables
	public float dashForce = 5f; 
	public float thrustTime = 1f; 
	private float thrustElapsed;
	public float dragTime = 0.5f; 
	private float dragElapsed;
	public float chargeTime = 1f;
	private float chargeElapsed;
	private bool dashCharged = true;
	
	public float dashDrag = 0.2f; 
	public float decelDrag = 2.5f; 
	private float baseDrag;

	
	//Requirement / Incompatables Lists
	private PCS.M[] incompatablesList = new PCS.M[1]{ 
		PCS.M.OnHit 
	}; 
	private PCS.M[] requiredsList = new PCS.M[0]{ 
		//PCS.M.Jump 
	};

	//Properties
	public override PCS.M MyID { get{return myID;} }

	// [[ ----- START ----- ]]
	private void Start(){
		baseDrag = mpcRef.GetComponent<Rigidbody2D>().drag; 
	}

// -----------------------------------------------------------------------------
// MPC-Called Methods

	// [[ ----- CHECK TRIGGER ----- ]]
	public override bool CheckTrigger(){

		// Determine state + input based activation 
		if( mpcRef.GetInputDown("dash") ){ dashPressed = true; }
		if( mpcRef.GetInputUp("dash") ){ dashPressed = false; }

		// Triggers
		if( moveCompleted && dashPressed && state == DashState.Charged ){
			moveCompleted = false;
		}
		
		// determine if move is in progress
		if( !moveCompleted ){ isActive = true; }
		else{ isActive = false; }

		// Check compatability with other moves
		if( isActive ){ 
			// Check incompatables list against higher-priority moves
			isActive = !PCS.CheckIncompatabilities( 
				mpcRef.GetTriggeredBehaviors(), incompatablesList, (int)myID );
			if( !isActive ){ EndMove(); }
			else{ 
				// Check requireds list against all moves
				isActive = PCS.CheckRequirements( 
					mpcRef.GetTriggeredBehaviors(), requiredsList );
				if( !isActive ){ EndMove(); }
			}
		}

		return isActive; 
	}

	// [[ ----- APPLY MOVEMENT ----- ]]
	public override void ApplyMovement( Rigidbody2D _rb2d ){
		
		// initiate move
		if( state == DashState.Charged ){
			
			// init
			state = DashState.Thrust;
			mpcRef.sprite.GetComponent<SpriteRenderer>().color = coolColor;

			// set drag
			_rb2d.drag = dashDrag; 

			// apply movement
			Vector2 dashDir = ModularPlayerController.reference.GetFacingVector();
			dashDir.Normalize();
			_rb2d.velocity = dashDir * dashForce; 
		}

		// apply drag
		else if( state == DashState.Drag && !dragApplied ){
			dragApplied = true; 
			_rb2d.drag = decelDrag;
		}
	}

	// [[ ----- NOMINATE ANIM ----- ]]
	public override PCS.A NominateAnim(){
		// if no animation associated w/ this move, return PCS.A.TOTAL
		
		PCS.A currAnim = PCS.A.TOTAL;
		

		return currAnim;   
	}

// -----------------------------------------------------------------------------
// Other Methods

	// [[ ----- UPDATE ----- ]]
	private void Update(){

		switch( state ){
			case DashState.Thrust:
				thrustElapsed += Time.deltaTime; 
				if( thrustElapsed > thrustTime ){ 
					state = DashState.Drag; 
					thrustElapsed = 0; 
				}
				break;
			
			case DashState.Drag:
				dragElapsed += Time.deltaTime; 
				if( dragElapsed > dragTime ){ 
					state = DashState.Cooldown; 
					dragElapsed = 0; 
					EndMove(); 
				}
				break;
			
			case DashState.Cooldown:
				chargeElapsed += Time.deltaTime;
				// apply color change
				float ratio = chargeElapsed / chargeTime; 
				SpriteRenderer pcSprite = mpcRef.sprite.GetComponent<SpriteRenderer>();
				Color newColor = Color.Lerp( coolColor, basicColor, ratio ); 
				pcSprite.color = newColor; 

				if( chargeElapsed > chargeTime ){ 
					state = DashState.Charged; 
					chargeElapsed = 0; 
				}
				break;
			
			default:
				break;
		}
	}

	// [[ ----- END MOVE ----- ]]
	public override void EndMove(){
		ResetInputs();
		thrustElapsed = 0; 
		dragElapsed = 0; 
		dragApplied = false; 
		moveCompleted = true;
	}

	// [[ ----- RESET INPUTS ----- ]]
	private void ResetInputs(){
		
	}
}
}