﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

namespace U2DPlatformer {
/**
@brief Basic structure for MPC_MoveBehavior classes 

This is a template intended to be copied and edited when making a new Move 
Behavior to use with the ModularPlayerController class. Contains default code to
indicate typical usage within the MPC framework.

@author Jonny Aiello
@version 1.2.0
@date 5/22/17
*/ 
public class MB_OnHit : MPC_MoveBehavior {

	//Variables
	private PCS.M myID = PCS.M.OnHit;
	private bool isActive = false; 
	private bool moveCompleted = true;

	private Color basicColor = Color.white;
	public Color hitColor = Color.white;

	public float knockbackForce = 1f; 
	public float stunTime = 1f;
	private float stunElapsed;
	private float baseDrag; 

	private bool stunInitialzed;
	
	//Requirement / Incompatables Lists
	private PCS.M[] incompatablesList = new PCS.M[0]{ 
		//PCS.M.AirBrake 
	}; 
	private PCS.M[] requiredsList = new PCS.M[0]{ 
		//PCS.M.Jump 
	};


	//Properties
	public override PCS.M MyID { get{return myID;} }

	// [[ ----- START ----- ]]
	private void Start(){
		baseDrag = mpcRef.GetComponent<Rigidbody2D>().drag; 
	}

// -----------------------------------------------------------------------------
// MPC-Called Methods

	// [[ ----- CHECK TRIGGER ----- ]]
	public override bool CheckTrigger(){

		// Determine state + input based activation 
		

		// Triggers
		if( moveCompleted && mpcRef.GetState(PCS.S.OnHitState) ){
			moveCompleted = false;
		}
		
		// determine if move is in progress
		if( !moveCompleted ){ isActive = true; }
		else{ isActive = false; }

		// Check compatability with other moves
		if( isActive ){ 
			// Check incompatables list against higher-priority moves
			isActive = !PCS.CheckIncompatabilities( 
				mpcRef.GetTriggeredBehaviors(), incompatablesList, (int)myID );
			if( !isActive ){ EndMove(); }
			else{ 
				// Check requireds list against all moves
				isActive = PCS.CheckRequirements( 
					mpcRef.GetTriggeredBehaviors(), requiredsList );
				if( !isActive ){ EndMove(); }
			}
		}

		return isActive; 
	}

	// [[ ----- APPLY MOVEMENT ----- ]]
	public override void ApplyMovement( Rigidbody2D _rb2d ){
		if( !stunInitialzed && !moveCompleted ){
			stunInitialzed = true; 
			_rb2d.drag = baseDrag; 
			mpcRef.sprite.GetComponent<SpriteRenderer>().color = hitColor;

			// knockback
			Vector2 kbDirection = 
				transform.position - mpcRef.lastHitCollider.transform.position;
			kbDirection.Normalize();
			Vector2 knockBackVect = kbDirection * knockbackForce;
			_rb2d.velocity = knockBackVect;
		}
	}

	// [[ ----- NOMINATE ANIM ----- ]]
	public override PCS.A NominateAnim(){
		// if no animation associated w/ this move, return PCS.A.TOTAL
		
		PCS.A currAnim = PCS.A.TOTAL;
		

		return currAnim;   
	}

// -----------------------------------------------------------------------------
// Other Methods

	// [[ ----- UPDATE ----- ]]
	private void Update(){

		// dash timer
		if( stunInitialzed ){
			stunElapsed += Time.deltaTime; 
			if( stunElapsed > stunTime ){ EndMove(); }
		}
	}

	// [[ ----- END MOVE ----- ]]
	public override void EndMove(){
		ResetInputs();
		stunElapsed = 0f; 
		mpcRef.sprite.GetComponent<SpriteRenderer>().color = basicColor;
		mpcRef.SetState( PCS.S.OnHitState, false ); 
		stunInitialzed = false; 
		moveCompleted = true;
	}

	// [[ ----- RESET INPUTS ----- ]]
	private void ResetInputs(){
		
	}
}
}