﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

namespace U2DPlatformer {
/**
@brief Basic structure for MPC_MoveBehavior classes 

Manages the directional facing of the sprite.

@author Jonny Aiello
@version 1.2.0
@date 5/22/17
*/ 
public class MB_Aim : MPC_MoveBehavior {

	//Variables
	private PCS.M myID = PCS.M.Aim;
	private bool isActive = false; 
	private bool moveCompleted = true;

	private float hVal;
	private float vVal;
	private bool aimLockPressed;
	
	//Requirement / Incompatables Lists
	private PCS.M[] incompatablesList = new PCS.M[1]{ 
		PCS.M.AimLock 
	}; 
	private PCS.M[] requiredsList = new PCS.M[0]{ 
		//PCS.M.Jump 
	};


	//Properties
	public override PCS.M MyID { get{return myID;} }

// -----------------------------------------------------------------------------
// MPC-Called Methods

	// [[ ----- CHECK TRIGGER ----- ]]
	public override bool CheckTrigger(){

		// Determine state + input based activation 
		hVal = mpcRef.GetAxis("h");
		vVal = mpcRef.GetAxis("v"); 

		// Triggers
		if( (aimLockPressed || hVal != 0 || vVal != 0) ){
			moveCompleted = false;
		}else{ moveCompleted = true; }
		
		// determine if move is in progress
		if( !moveCompleted ){ isActive = true; }
		else{ isActive = false; }

		// Check compatability with other moves
		if( isActive ){ 
			// Check incompatables list against higher-priority moves
			isActive = !PCS.CheckIncompatabilities( 
				mpcRef.GetTriggeredBehaviors(), incompatablesList, (int)myID );
			if( !isActive ){ EndMove(); }
			/*else{ 
				// Check requireds list against all moves
				isActive = PCS.CheckRequirements( 
					mpcRef.GetTriggeredBehaviors(), requiredsList );
				if( !isActive ){ EndMove(); }
			}*/
		}

		return isActive; 
	}

	// [[ ----- APPLY MOVEMENT ----- ]]
	public override void ApplyMovement( Rigidbody2D _rb2d ){
		
		Transform sprite = mpcRef.sprite.transform;
		PCS.Dir facingDir = PCS.Dir.North; 

		// Determine Facing
		if( (hVal != 0) || (vVal != 0) ){
			if( vVal > 0 && hVal == 0 ){ facingDir = PCS.Dir.North; }
			else if( vVal > 0 && hVal > 0 ){ facingDir = PCS.Dir.NorthEast; }
			else if( vVal == 0 && hVal > 0 ){ facingDir = PCS.Dir.East; }
			else if( vVal < 0 && hVal > 0 ){ facingDir = PCS.Dir.SouthEast; }
			else if( vVal < 0 && hVal == 0 ){ facingDir = PCS.Dir.South; }
			else if( vVal < 0 && hVal < 0 ){ facingDir = PCS.Dir.SouthWest; }
			else if( vVal == 0 && hVal < 0 ){ facingDir = PCS.Dir.West; }
			else if( vVal > 0 && hVal < 0 ){ facingDir = PCS.Dir.NorthWest; }
		}

		// Set facing
		mpcRef.facingDir = facingDir;
		sprite.rotation = PCS.GetQuatFromDir( facingDir );
	}

	// [[ ----- NOMINATE ANIM ----- ]]
	public override PCS.A NominateAnim(){
		// if no animation associated w/ this move, return PCS.A.TOTAL
		
		PCS.A currAnim = PCS.A.TOTAL;
		

		return currAnim;   
	}

// -----------------------------------------------------------------------------
// Other Methods

	// [[ ----- END MOVE ----- ]]
	public override void EndMove(){
		ResetInputs();
		moveCompleted = true;
	}

	// [[ ----- RESET INPUTS ----- ]]
	private void ResetInputs(){
		
	}
}
}