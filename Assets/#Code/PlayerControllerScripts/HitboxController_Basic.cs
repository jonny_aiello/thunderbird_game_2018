﻿using UnityEngine;
using System.Collections;

namespace U2DPlatformer {
/**
@brief Detects collisions and triggers logic in MPC 

Each PC collider should be kept on its own GO, and each of those GOs should have
their own version of this script. Detects collisions and then calls appropriate
logic in MPC through public methods.

_Component of:_
<br> PC Collider game objects

_Class output:_
<br> Calls methods on ModularPlayerController

_Class input:_
<br> Triggered by OnCollision Unity events

@author Jonny Aiello
@version 1.2.0
@date 5/22/17
*/ 
public class HitboxController_Basic : MonoBehaviour {

	// Reference Variables
	public ModularPlayerController mpc;

// -----------------------------------------------------------------------------
// Unity Events

	// [[ ----- ON TRIGGER ENTER 2D ----- ]]
	private void OnTriggerEnter2D( Collider2D _c ){
		if( gameObject.activeSelf ){
			// sort collision based on layer
			switch( _c.gameObject.layer ){
				
				// Player layer
				case 8:
					break;
				
				// Enemy layer
				case 9:
					mpc.OnHit( _c );
					break;
				
				// Bullet_player layer
				case 10:
					Debug.Log("CollisionEnter: layer = " + 10);
					break;
				
				// Bullet_enemy layer
				case 11:
					Debug.Log("CollisionEnter: layer = " + 11);
					break;
				default:
					Debug.Log("switch: value match not found");
					break;
			}
		}
	}

	// [[ ----- ON TRIGGER EXIT 2D ----- ]]
	private void OnTriggerExit2D( Collider2D _c ){
		if( gameObject.activeSelf ){
		// sort collision based on layer
			switch( _c.gameObject.layer ){
				case 8:
					Debug.Log("CollisionEnter: layer = " + 8);
					break;
				case 9:
					// Jumpable layer
					
					// set grounded
					/*if(GetLowestContact(_c.contacts).y < transform.position.y ){
						mpc.SetIsGrounded( false ); 
					}*/
				
					// Debug.Log("CollisionEnter: layer = " + 9);
					break;
				case 10:
					Debug.Log("CollisionEnter: layer = " + 10);
					break;
				default:
					Debug.Log("switch: value match not found");
					break;
			}
		}
	}

	// [[ ----- ON TRIGGER STAY 2D ----- ]]
	private void OnTriggerStay2D( Collider2D _c ){
		if( gameObject.activeSelf ){
			// sort collision based on layer
			switch( _c.gameObject.layer ){
				
				// Enemy layer
				case 9:
					mpc.OnHit( _c );
					break;
			
				default:
					Debug.Log("switch: value match not found");
					break;
			}
		}
	}

// -----------------------------------------------------------------------------
// Private Methods

	// [[ ----- GET LOWEST CONTACT ----- ]]
	private Vector2 GetLowestContact( ContactPoint2D[] _contacts ){
		Vector2 lowest = new Vector2( 0, Mathf.Infinity ); 
		foreach( ContactPoint2D c in _contacts ){
			if( c.point.y < lowest.y ){ lowest = c.point; }
		}
		return lowest;
	}
}
}