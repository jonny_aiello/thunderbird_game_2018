﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

namespace U2DPlatformer {
/**
@brief Basic structure for MPC_MoveBehavior classes 

This is a template intended to be copied and edited when making a new Move 
Behavior to use with the ModularPlayerController class. Contains default code to
indicate typical usage within the MPC framework.

@author Jonny Aiello
@version 1.2.0
@date 5/22/17
*/ 
public class MB_TEMPLATE : MPC_MoveBehavior {

	//Variables
	private PCS.M myID = PCS.M.Idle;
	private bool isActive = false; 
	private bool moveCompleted = true;
	
	//Requirement / Incompatables Lists
	private PCS.M[] incompatablesList = new PCS.M[0]{ 
		//PCS.M.Run 
	}; 
	private PCS.M[] requiredsList = new PCS.M[0]{ 
		//PCS.M.Jump 
	};


	//Properties
	public override PCS.M MyID { get{return myID;} }

// -----------------------------------------------------------------------------
// MPC-Called Methods

	// [[ ----- CHECK TRIGGER ----- ]]
	public override bool CheckTrigger(){

		// Determine state + input based activation 

		// Triggers
		
		// determine if move is in progress
		if( !moveCompleted ){ isActive = true; }
		else{ isActive = false; }

		// Check compatability with other moves
		if( isActive ){ 
			// Check incompatables list against higher-priority moves
			isActive = !PCS.CheckIncompatabilities( 
				mpcRef.GetTriggeredBehaviors(), incompatablesList, (int)myID );
			if( !isActive ){ EndMove(); }
			else{ 
				// Check requireds list against all moves
				isActive = PCS.CheckRequirements( 
					mpcRef.GetTriggeredBehaviors(), requiredsList );
				if( !isActive ){ EndMove(); }
			}
		}

		return isActive; 
	}

	// [[ ----- APPLY MOVEMENT ----- ]]
	public override void ApplyMovement( Rigidbody2D _rb2d ){

	}

	// [[ ----- NOMINATE ANIM ----- ]]
	public override PCS.A NominateAnim(){
		// if no animation associated w/ this move, return PCS.A.TOTAL
		
		PCS.A currAnim = PCS.A.TOTAL;
		

		return currAnim;   
	}

// -----------------------------------------------------------------------------
// Other Methods

	// [[ ----- END MOVE ----- ]]
	public override void EndMove(){
		ResetInputs();
		moveCompleted = true;
	}

	// [[ ----- RESET INPUTS ----- ]]
	private void ResetInputs(){
		
	}
}
}