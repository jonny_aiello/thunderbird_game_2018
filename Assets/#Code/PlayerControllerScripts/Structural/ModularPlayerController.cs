﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 
using System.Linq;
using U2DProjectCore; 

namespace U2DPlatformer {
/**
@brief Modular Player Character Controller - Determines PC actions based on 
controller input 

Takes input from the game pad and external game factors to modify the player 
character's internal states. Based on those states, determines the physics to 
apply for the next physics update, as well as what changes to apply to the 
character's Animator.

@author Jonny Aiello
@version 1.2.0
@date 5/22/17
*/ 
[RequireComponent(typeof(Rigidbody2D))]
public class ModularPlayerController : TopDownUnit {

// -----------------------------------------------------------------------------
// Variables

	public static ModularPlayerController reference;

	public bool debugOn; 
	public bool drawGizmos; 
	public Color gizmoColor; 
	[HideInInspector] public PCS.Dir facingDir;

	// external variables
	public int startingHP = 5;  

	// internal variables
	private PCS.A lastAnim = PCS.A.TOTAL;
	private float timeAnimComplete;
	private float pupActivationDelay = 0.25f;
	private float pupActivationAvailable;
	private bool lastPupActivationState;
	[HideInInspector] public Collider2D lastHitCollider;

	// list variables
	private bool[] states = new bool[(int)PCS.S.TOTAL];
	
	// Reference Variables
	private Rigidbody2D rb2d; 
	// public GameObject sprite;  // inherits from TopDownUnit 
	private Animator animator;
	public Transform facePoint;
	public SceneController_base sceneController; 
	// public LayerMask platformLayer;
	
	// Serializable public var sets
	// public Bindings bindings;
	public MPC_AxisBinding[] axisBindings; 
	public MPC_ButtonBinding[] buttonBindings;
	public MPC_Animation[] animSlots;
	public MPC_MBSlot[] moveSlots; 
	
	// Loadout Variables
	private MPC_MoveBehavior[] defaultLoadout;


// -----------------------------------------------------------------------------
// Unity Events

	// [[ ----- AWAKE ----- ]]
	void Awake () {

		// pass refs for MoveBehaviors
		MPC_MoveBehavior.SetMpcReference( this ); 

		// init references
		reference = this; 
		rb2d = GetComponent<Rigidbody2D>(); 
		sprite = transform.Find("Sprite"); 
		animator = sprite.GetComponent<Animator>();  

		// EDIT STATES - initialize
		//states[(int)PCS.S.FacingRight] = true; 

		// Set up container classes
		foreach( MPC_MBSlot mbs in moveSlots ){ mbs.Initialize(); }
		
		// Check inspector slot setup for errors
		CheckMBSlotSetup();
		CheckAnimSlotSetup();

		OnSpawn();

		// init default mode loadout
		defaultLoadout = new MPC_MoveBehavior[moveSlots.Length];
		for( int i = 0; i < moveSlots.Length; i++ ){
			defaultLoadout[i] = moveSlots[i].GetMB(); 
		}
	}

	// [[ ----- ON DRAW GIZMOS ----- ]]
	/*public void OnDrawGizmos(){
 
		if( drawGizmos ){ 
			// ground check
			Gizmos.color = gizmoColor; 
			Gizmos.DrawSphere( groundCheck.position, groundCheckRadius );
			// AlmostLedgedCheck
			Gizmos.DrawLine( 
				new Vector2(transform.position.x, almostLedgedCheck.position.y), 
				almostLedgedCheck.position );
			// FoundWallCheck
			Gizmos.DrawLine( transform.position, foundWallCheck1.position ); 
			Gizmos.DrawLine( transform.position, foundWallCheck2.position ); 
		}
	}*/
	
	// [[ ----- UPDATE ----- ]]
	void Update () {

		SetInputs(); 
		CheckTriggers();
	}

	// [[ ----- FIXED UPDATE ----- ]]
	void FixedUpdate () {

		ApplyMoves();
		SetAnimState();
		SetStates();
		
	}

// -----------------------------------------------------------------------------
// Internal Player Controller Methods

	// [[ ----- SET INPUTS ----- ]]
	private void SetInputs(){
		foreach( MPC_AxisBinding ab in axisBindings ){ ab.GetInputState(); }
		foreach( MPC_ButtonBinding bb in buttonBindings ){ bb.GetInputState(); }
	}

	// [[ ----- CHECK TRIGGERS ----- ]]
	private void CheckTriggers(){

		foreach( MPC_MBSlot mbs in moveSlots ){ mbs.CheckSlotTrigger(); }

		// print
		if( debugOn ){
			string output = "Moves Triggered: \n"; 
			foreach( MPC_MBSlot mbs in moveSlots.Where(x=>x.moveTriggered) ){
				output += mbs.slotType.ToString() + "\n"; 
			}
		}
	}

	// [[ ----- APPLY MOVES ----- ]]
	private void ApplyMoves(){

		foreach( MPC_MBSlot mbs in moveSlots ){ 
			mbs.ApplySlotMove( rb2d ); 

			// nominate anim
			if( mbs.moveTriggered ){
				PCS.A myNomination = mbs.GetAnimNomination();

				if( myNomination != PCS.A.TOTAL ){ 
					MPC_Animation nominee = animSlots
						.Where( x=> x.animType == myNomination )
						.Single();
					nominee.isNominated = true; 
				}
			}
		}

		// print logs
		/*if( debugOn ){
			string output = "Nominated Anims:\n";
			foreach( MPC_Animation a in animSlots ){
				if( a.isNominated ){ output += a.animType + "\n"; }
			}
			Debug.Log(output);
		}*/
	}


	// [[ ----- SET ANIM STATE ----- ]]
	private void SetAnimState(){

		// SET NEW ANIMATIONS
		 
		// Only swap animations if last one has completed
		// most durations are 0, if an animation needs to complete change its 
		// duration
		if( Time.time > timeAnimComplete ){
			var challengers = animSlots
			.Where(x => x.isNominated && (int)x.animType < (int)lastAnim );

			if( challengers.Count() > 0 ){
				// change animation
				// get highest priority of potentials
				int smallestIndex = challengers.Min(x=> (int)x.animType );
				MPC_Animation newAnim = challengers
					.Where( x => (int)x.animType == smallestIndex )
					.Single();
				// set animation
				lastAnim = newAnim.animType;
				timeAnimComplete = Time.time + newAnim.duration;
				animator.Play( newAnim.clipName ); 

			}else{
				// reset last anim to allow lower priority anims a shot
				lastAnim = PCS.A.TOTAL; 	
			}
		}

		foreach( MPC_Animation myAnim in animSlots ){ 
			myAnim.isNominated = false; 
		}
	}

	// [[ ----- SET STATES ----- ]]
	private void SetStates(){

		// EDIT STATES bindings go here
		

		// IsGrounded - overlap circle
		/*bool lastGrounded = states[(int)PCS.S.IsGrounded];
		states[(int)PCS.S.IsGrounded] = Physics2D.OverlapCircle(
			groundCheck.position,
			groundCheckRadius,
			platformLayer
			);
*/
		/*if( states[(int)PCS.S.IsGrounded] != lastGrounded ){ 
			Debug.Log("GROUNDED: " + states[(int)PCS.S.IsGrounded]);
			} */

		// Airborne
		/*states[(int)PCS.S.Airborne] = !Physics2D.Linecast(
			transform.position, 
			airCheck.position, 
			boundaryLayer 
			);
		*/
		/*Debug.Log("airborn: " + states[(int)PCS.S.Airborne]);*/

		// AlmostLedged
		/*Vector2 kneeHeight = almostLedgedCheck.position; 
		kneeHeight.x = transform.position.x; 
		bool nearLedge = Physics2D.Linecast(
			kneeHeight, 
			almostLedgedCheck.position, 
			platformLayer 
			);
		states[(int)PCS.S.AlmostLedged] = 
			( nearLedge && !states[(int)PCS.S.IsGrounded] ); 

		// Found Wall
		bool topWallCheck = Physics2D.Linecast(
			transform.position, 
			foundWallCheck1.position, 
			platformLayer 
			);
		bool bottomWallCheck = Physics2D.Linecast(
			transform.position,
			foundWallCheck2.position,
			platformLayer
		);
		states[(int)PCS.S.FoundWall] = ( topWallCheck || bottomWallCheck ); 
		*/	
		// print active states
		if( debugOn ){ 
			string sa = "";
			for( int i = 0; i < states.Length; i++ ){
				if( states[i] ){ sa += ((PCS.S)i + ", "); }
			}
			Debug.Log("Active States: " + sa);
		}
	}

	/**@brief Checks passed loadout against current loaded MBs, updates any that
	 differ */ 
	// [[ ----- UPDATE LOADOUT ----- ]]
	private void UpdateLoadout( MPC_MoveBehavior[] _loadout ){

		for( int i = 0; i < _loadout.Length; i++ ){
			if( _loadout[i] != null ){ 

				// deactivate the pre-swapped move if it's active		
				foreach( MPC_MoveBehavior mb in moveSlots
					.Where(x=> x.moveTriggered && x.slotType == _loadout[i].MyID)
					.Select(x=> x.MBComponent ) ){

					mb.EndMove();
				}

				// Debug
				if( debugOn ){ 
					// Debug.Log(System.String.Format(
					// 	"move: {0} replaced with: {1}", moves[id], _loadout[i])
					// );
				}
				
				MPC_MBSlot swapSlot = moveSlots
					.Where(x=> x.slotType == _loadout[i].MyID)
					.Single();
				swapSlot.MBComponent = _loadout[i];
			}
		}
	}

// -----------------------------------------------------------------------------
// Private Methods

	/* internal logic not pertaining directly to the MB systems. Ex - logic 
	triggered by player collisions */

	// [[ ----- GET NEW POWERUP ----- ]]
	/*private void GetNewPowerup( GameObject cGO, PowerupPlatform puP ){
		// if script is attached + player is performing DownSmash
		if( puP.IsAvailable && movesTriggered[(int)PCS.M.DownSmash] ){
			
			// trigger puP logic and charactermodecontroller logic
			powerupCMC.NewMode();
			puP.PlatformOff();
		} 
	}*/

	// [[ ----- CHECK MB SLOT SETUP ----- ]]
	private void CheckMBSlotSetup(){
		string errorInstruction = "\n add a single slot in the" 
			+ " ModularPlayerController's inspector for each type in PCS.M";
		foreach( PCS.M m in System.Enum.GetValues(typeof(PCS.M)) ){
			int matchingSlotsNum = moveSlots.Count(x => x.slotType == m);
			if( matchingSlotsNum == 0 ){
				Debug.LogError("MB Slot Error: missing a slot for " + m 
					+ errorInstruction);
			}else if( matchingSlotsNum > 1 ){
				Debug.LogError("MB Slot Error: more than 1 slot found for " + m
					+ errorInstruction);
			}
		}
	}

	// [[ ----- CHECK ANIM SLOT SETUP ----- ]]
	private void CheckAnimSlotSetup(){
		foreach( PCS.A a in System.Enum.GetValues(typeof(PCS.A)) ){
			if( a == PCS.A.TOTAL ){ continue; }
			if( animSlots.Count(x=> x.animType == a) > 1 ){
				Debug.LogError("Anim Slot Error: more than one slot found for "
					+ a);
			}else if( animSlots.Count(x=> x.animType == a) < 1 ){
				Debug.LogError("Anim Slot Error: slot missing for " + a);
			}
		}
		if( animSlots.Count(x=> System.String.IsNullOrEmpty(x.clipName)) > 0 ){
			Debug.LogError("Anim Slot Error: all slots need clipName assigned");
		}
	}

// -----------------------------------------------------------------------------
// State Change Methods
	/**@brief IsGrounded is set by the HitboxController script on collider GO */ 
	// [[ ----- SET IS GROUNDED ----- ]]
	/*public void SetIsGrounded( bool _isEnabled ){
		states[(int)PCS.S.IsGrounded] = _isEnabled; 
	}*/

// -----------------------------------------------------------------------------
// Public Getter Methods - States/Inputs/TriggeredBehaviors

	// [[ ----- GET INPUT UP ----- ]]
	public bool GetInputUp( string _label ){
		try{
			bool isUp = buttonBindings
				.Where( b => b.label == _label )
				.Select( b => b.isReleased )
				.Single();
			return isUp;
		}catch{ 
			Debug.LogError("Input label not found in buttonBindings: " +_label);
			return false; 
		}	
	}

	// [[ ----- GET INPUT DOWN ----- ]]
	public bool GetInputDown( string _label ){
		try{
			bool isDown = buttonBindings
				.Where( b => b.label == _label )
				.Select( b => b.isPressed )
				.Single();
			return isDown;
		}catch{ 
			Debug.LogError("Input label not found in buttonBindings: " +_label);
			return false; 
		}	
	}

	// [[ ----- GET INPUT ----- ]]
	public bool GetInput( string _label ){
		try{
			bool isPressed = buttonBindings
				.Where( b => b.label == _label )
				.Select( b => b.isPressed )
				.Single();
			return isPressed;
		}catch{ 
			Debug.LogError("Input label not found in buttonBindings: " +_label);
			return false; 
		}	
	}

	// [[ ----- GET AXIS ----- ]]
	public float GetAxis( string _label ){
		try{
			float axisValue = axisBindings
				.Where( b => b.label == _label )
				.Select( b => b.value )
				.Single();
			return axisValue;
		}catch{ 
			Debug.LogError("Input label not found in axisBindings: " +_label);
			return 0; 
		}
	}

	// [[ ----- GET STATE ----- ]]
	public bool GetState( PCS.S _state ){
		return states[(int)_state]; 
	}

	// [[ ----- SET STATE ----- ]]
	public void SetState( PCS.S _state, bool _value ){
		states[(int)_state] = _value; 
	}
	
	// [[ ----- GET TRIGGERED BEHAVIORS ----- ]]
	public MPC_MBSlot[] GetTriggeredBehaviors(){ return moveSlots; }

	// [[ ----- IS BEHAVIOR TRIGGERED ----- ]]
	public bool IsBehaviorTriggered( PCS.M _move ){
		bool moveTriggered = moveSlots
			.Where( mb => mb.slotType == _move )
			.Select( mb => mb.moveTriggered )
			.SingleOrDefault();

		return moveTriggered; 
	}

// -----------------------------------------------------------------------------
// Public Methods

	// [[ ----- GET SHOOT POINT ----- ]]
	public Vector2 GetShootPoint(){
		Vector2 hackPos = (Vector2)facePoint.position + new Vector2( -1, 0 ); 
		return hackPos;
	}

	// [[ ----- GET FACING VECTOR ----- ]]
	public Vector2 GetFacingVector(){
		return facePoint.position - transform.position; 
	}

	// [[ ----- SET MODE ----- ]]
	public void SetMode( MPC_MoveBehavior[] _loadout ){
		UpdateLoadout( _loadout ); 
	}

	// [[ ----- RESET MODE ----- ]]
	public void ResetMode(){
		SetMode( defaultLoadout ); 
	}

	// [[ ----- ON SPAWN ----- ]]
	public override void OnSpawn(){
		currentHP = maxHP = startingHP;
	}

	// [[ ----- ON HIT ----- ]]
	public void OnHit( Collider2D _c ){
		// Debug.Log("HIT");
		if( !states[(int)PCS.S.OnHitState] ){
			states[(int)PCS.S.OnHitState] = true; 
			lastHitCollider = _c;
			// take damage
			currentHP -= 1;
			Debug.Log("PC Hitpoints: " + currentHP);
			if( currentHP <= 0 ){ 
				Invoke("OnDeath", 0.5f); 
			}
		}
	}

	// [[ ----- ON DEATH ----- ]]
	public override void OnDeath(){
		sceneController.OnPCDeath();
	}
}
}