﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 
using System.Linq; 

namespace U2DPlatformer {
/**
@brief Interface for Move Behavior scripts used in ModularPlayerController 

Every Move Behavior class (generally prefaced with "MB_") extends from this 
abstract class. Move Behaviors are scripts that contain specific logic for a 
player action (jump, dash, shoot, etc) and are plugged into the 
ModularPlayerController script as needed.

@author Jonny Aiello
@version 1.2.1
@date 5/22/17
*/ 
public abstract class MPC_MoveBehavior : MonoBehaviour {

	// Variables
	protected static ModularPlayerController mpcRef;

	// Properties
	public abstract PCS.M MyID { get; }

	// Reference Variables
	protected MPC_ButtonBinding[] buttons;
	protected MPC_AxisBinding[] axes;

// -----------------------------------------------------------------------------
// Public Methods

	// [[ ----- SET MPC REFERENCE ----- ]]
	public static void SetMpcReference( ModularPlayerController _mpc ){
		mpcRef = _mpc; 
	}


// -----------------------------------------------------------------------------
// Abstract Methods
	
	public abstract bool CheckTrigger(); 
	public abstract void ApplyMovement( Rigidbody2D _rb2d ); 
	public abstract PCS.A NominateAnim(); 
	public abstract void EndMove();

}
}