﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic; 

namespace U2DPlatformer {
/**
@brief Organizes modes, sends modes to ModularPlayerController 

A "mode" is a loadout of move behaviors that will overwrite those currently held
within ModularPlayerController. For organizational pruproses, modes are kept 
within their own GameObject, that contains a CMC_CharacterMode component, along
with each of the MPC_MoveBehavior components used in that mode. This GameObject
is then childed to an organizational GameObject containing a 
CharacterModeController component, and that GameObject is childed to the 
ModularPlayerController GameObject.
<br>
CharacterModeController holds a dictionary of each of the character modes and 
has public methods setting a mode to the ModularPlayerController and resetting 
the default loadout of move behaviors.
<br>
This version is constructed to handle a single-use power-up system, so it has
a variable for the currently held powerup, and logic for randomly choosing a 
powwer-up mode from those available.

_Class output:_
<br> Sends a list of move behaviors associated with a chosen mode to 
ModularPlayerController.SetMode()

_Class input:_
<br> Mode activation and deactivation are handled through public methods by 
MPC_MoveBehavior scripts running in ModularPlayerController

@author Jonny Aiello
@version 1.2.0
@date 5/22/17
*/ 
public class CharacterModeController : MonoBehaviour {

	// Enums
	// EDIT MODES
	public enum ModeType{
		WalkOnly,
		TOTAL
	}

	// Variables
	public ModeType modeSelect;
		/**< if set to anything besides TOTAL, will only set selected mode */
	public ModeType currentMode = ModeType.TOTAL;
	private Dictionary<ModeType, CMC_CharacterMode> modeDic 
		= new Dictionary<ModeType, CMC_CharacterMode>();
	private bool playerTextLock;

	// Properties
	public ModeType CurrentMode { get{return currentMode;} }

	// Reference Variables
	public ModularPlayerController playerController;
	public Text playerText;

// -----------------------------------------------------------------------------
// Unity Events

	// [[ ----- AWAKE ----- ]]
	private void Awake(){
		// add attached modes to dictionary
		foreach( Transform child in transform ){
			CMC_CharacterMode mode = child.GetComponent<CMC_CharacterMode>();
			modeDic.Add( mode.modeType, mode ); 
		}
	}

// -----------------------------------------------------------------------------
// Private Methods

	// [[ ----- SET CURRENT MODE ----- ]]
	private void SetCurrentMode(){
		ModeType selectedMode = ModeType.TOTAL;

		// if modeSelect = TOTAL, normal random selection
		if( modeSelect == ModeType.TOTAL ){
			int selectIndex = Random.Range( 0, (int)ModeType.TOTAL ); 
			selectedMode = (ModeType)selectIndex; 
		}
		// else return mode specified by modeSelect
		else{
			selectedMode = modeSelect;
		}

		// update currentMode and set text
		currentMode = modeDic[selectedMode].modeType;
		StartCoroutine( "SetPlayerText", modeDic[currentMode].setText ); 
	}

	// [[ ----- ACTIVATE MODE ----- ]]
	private void ActivateMode( ModeType _mode ){
		StartCoroutine( "SetPlayerText", modeDic[_mode].executeText );
		playerController.SetMode( modeDic[_mode].moveBehaviors ); 
	}

// -----------------------------------------------------------------------------
// Public Methods

	// [[ ----- NEW MODE ----- ]]
	public void NewMode(){
		if( currentMode == ModeType.TOTAL ){ 
			SetCurrentMode();
			Debug.Log("TESTING - New mode set!");
		}
	}

	// [[ ----- CURRENT MODE ON ----- ]]
	public void CurrentModeOn(){
		if( currentMode != ModeType.TOTAL ){ ActivateMode( currentMode ); }
	}

	// [[ ----- CURRENT MODE OFF ----- ]]
	public void CurrentModeOff(){
		playerController.ResetMode();
		currentMode = ModeType.TOTAL;
	}

	// [[ ----- FLASH TEXT ----- ]]
	public void FlashText( string _text ){
		StartCoroutine("SetPlayerText", _text);
	}

// -----------------------------------------------------------------------------
// Coroutines

	// [[ ----- SET PLAYER TEXT ----- ]]
	private IEnumerator SetPlayerText( string _text ){
		if( !playerTextLock ){ 
			playerTextLock = true;
			string lastText = playerText.text;
			playerText.text = _text;
			yield return new WaitForSeconds(1f); 
			playerText.text = lastText; 
			playerTextLock = false; 
		}
	}
}
}