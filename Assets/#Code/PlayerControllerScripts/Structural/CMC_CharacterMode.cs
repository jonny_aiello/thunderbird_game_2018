﻿using UnityEngine;
using System.Collections;

namespace U2DPlatformer {
/**
@brief Holds an array of MPC_MoveBehaviors associated with a specfic character 
mode or power-up state 

A "mode" is a loadout of move behaviors that will overwrite those currently held
within ModularPlayerController. For organizational pruproses, modes are kept 
within their own GameObject, that contains a CMC_CharacterMode component, along
with each of the MPC_MoveBehavior components used in that mode. This GameObject
is then childed to an organizational GameObject containing a 
CharacterModeController component, and that GameObject is childed to the 
ModularPlayerController GameObject.
<br>
A CMC_CharacterMode script is attached to a GameObject representing a mode - 
these could be power-up states, character transformation states, etc. Every 
move behavior employed or changed in this state needs to be added as a component
to this GameObject, then a hard link for it must be added to the mbSlots list in
the Unity inspector. On Awake(), this script casts the array to MPC_MoveBehavior
 array.

_Class output:_
<br> none

_Class input:_
<br> CharacterModeController will reference the move behaviors contained here

@author Jonny Aiello
@version 1.2.0
@date 5/22/17
*/ 
public class CMC_CharacterMode : MonoBehaviour {

	public CharacterModeController.ModeType modeType;
	public string setText;
	public string executeText; 
	public MPC_MoveBehavior[] moveBehaviors;
}
}