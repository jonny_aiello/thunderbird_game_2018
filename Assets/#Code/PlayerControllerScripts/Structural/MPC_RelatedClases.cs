﻿using UnityEngine;
using System.Collections;
using System.Linq;

namespace U2DPlatformer {
/**@brief PCS - "Player Controller Scope", publically referenceable enums and
static methods used within the EPCController and its associated classes */ 
public static class PCS {
// -----------------------------------------------------------------------------
// PCS - Enums	

	/**@brief for a move to have incompatability with another move, it must be 
	lower on the list than the move it is incompatable with */ 
	// Moves
	// EDIT MOVES list here
	public enum M {
		/*Hurt,*/ 
		/*WallJump,*/
		/*PowerUp,*/
		// Jump,
		// Run,
		// Walk,
		OnHit,
		Dash,
		AirBrake,
		AimLock,
		Aim,
		Thrust,
		Shoot,
		Idle
	}
	/**@brief order of states is inconsequential */ 
	// States
	// EDIT STATES list here
	public enum S {
		/*HurtStun,*/
		/*WalledFront,*/
		/*WalledBack,*/
		// IsGrounded,
		// IsAirborne,
		// AlmostLedged,
		// FoundWall,
		// FacingRight,
		OnHitState,
		AirBraking,
		TOTAL
	}
	/**@brief the order of this list determines animation priority, highest at
	the top and lowest at the bottom */ 
	// Animations
	// EDIT ANIMATIONS list here
	public enum A {
		/*Jump,
		Fall,*/
		// Jump,
		// Run,
		Idle,
		TOTAL
	}

	// Directions
	public enum Dir {
		North,
		NorthEast,
		East,
		SouthEast,
		South,
		SouthWest,
		West,
		NorthWest
	}

	// [[ ----- GET QUAT FROM DIR ----- ]]
	public static Quaternion GetQuatFromDir( Dir _direction ){
		float newAngle = 0f; 
		switch( _direction ){
			case PCS.Dir.North:
				newAngle = 180f;
				break;
			case PCS.Dir.NorthEast:
				newAngle = 135f;
				break;
			case PCS.Dir.East:
				newAngle = 90f;
				break;
			case PCS.Dir.SouthEast:
				newAngle = 45f;
				break;
			case PCS.Dir.South:
				newAngle = 0;
				break;
			case PCS.Dir.SouthWest:
				newAngle = 315f;
				break;
			case PCS.Dir.West:
				newAngle = 270f;
				break;
			case PCS.Dir.NorthWest:
				newAngle = 225;
				break;
			default:
				Debug.Log("switch: value match not found");
				break;
		}
		return Quaternion.Euler( 0, 0, newAngle );
	}

// -----------------------------------------------------------------------------
// PCS - List Comparison Functions
		
	/**@brief if any triggered move matches an item on the incompatabilites
	list, return true. Otherwise, return false. Only check moves with higher
	priority than the calling move. */ 
	// [[ ----- CHECK INCOMPATABILITIES ----- ]]
	public static bool CheckIncompatabilities( 
		MPC_MBSlot[] _moves, PCS.M[] _incompatables, int _myID ){

		// get list of PCS.Ms that are triggered and higher-priority
		var activeList = _moves
			.Where( x => x.moveTriggered && (int)x.slotType < _myID )
			.Select( x => x.slotType );

		//  find any matches within incompatables list
		var matchesFound = from activeMove in activeList
			join incompMove in _incompatables
			on activeMove equals incompMove
			select activeMove; 

		if( matchesFound.Count() > 0 ){ return true; }
		else{ return false; } 
	}

	// [[ ----- CHECK REQUIREMENTS ----- ]]
	public static bool CheckRequirements( 
		MPC_MBSlot[] _moves, PCS.M[] _requireds ){

		for( int i = 0; i < _requireds.Length; i++ ){
			int movesCount = _moves.Count(
				x=> x.moveTriggered && x.slotType == _requireds[i] );
			if( movesCount == 0 ){ return false; }
		}
		return true; 
	}
}

// -----------------------------------------------------------------------------
// MPC Container Objects

// --- 
[System.Serializable]
public class MPC_ButtonBinding {

	// Variables
	public string label; 
	public string inputManagerId; 
	public bool isPressed = false; 
	public bool isReleased = false;

	// [[ ----- GET INPUT STATE ----- ]]
	public void GetInputState(){
		isPressed = Input.GetButtonDown( inputManagerId );
		isReleased = Input.GetButtonUp( inputManagerId ); 
	}
}

[System.Serializable]
public class MPC_AxisBinding {

	// Variables
	public string label; 
	public string inputManagerId; 
	public float value; 

	// [[ ----- GET INPUT STATE ----- ]]
	public void GetInputState(){ value = Input.GetAxisRaw( inputManagerId ); }
}

//--- MBSlot
[System.Serializable]
public class MPC_MBSlot{

	// Variables
	public string label; // helps navigate inspector, that's all
	public PCS.M slotType;
	public MonoBehaviour MBComponent;
	public bool moveTriggered; 

	// [[ ----- INITIALIZE ----- ]]
	public void Initialize(){
		// test compatability with loaded MB
		if( MBComponent != null ){
			MPC_MoveBehavior includedMB = (MPC_MoveBehavior)MBComponent;
			if( includedMB.MyID != slotType ){
				Debug.LogError("Mismatched MoveBehavior type in slot: " 
					+ slotType +"\n MoveBehavior.MyID must match slot!");
			}
		}else{
			Debug.LogWarning("MoveBehavior slot is empty: " + slotType);
		}
	}

	// [[ ----- CHECK SLOT TRIGGER ----- ]]
	public void CheckSlotTrigger(){

		MPC_MoveBehavior includedMB = (MPC_MoveBehavior)MBComponent;
		moveTriggered = includedMB.CheckTrigger();
	}

	// [[ ----- APPLY SLOT MOVE ----- ]]
	public void ApplySlotMove( Rigidbody2D _rb2d ){
		if( moveTriggered ){ GetMB().ApplyMovement( _rb2d ); }
	}

	// [[ ----- GET ANIM NOMINATION ----- ]]
	public PCS.A GetAnimNomination(){ return GetMB().NominateAnim(); }

	// [[ ----- GET MB ----- ]]
	public MPC_MoveBehavior GetMB(){ return (MPC_MoveBehavior)MBComponent; }
}

//---
[System.Serializable]
public class MPC_Animation{

	// Variables
	public PCS.A animType;
	public string clipName;  
	public bool isNominated;
	public float duration; 
		// in seconds
}

}
