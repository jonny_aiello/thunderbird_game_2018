using UnityEngine;
using System.Collections;

namespace U2DPlatformer {
/**
@brief Detects collisions and triggers logic in MPC 

Each PC collider should be kept on its own GO, and each of those GOs should have
their own version of this script. Detects collisions and then calls appropriate
logic in MPC through public methods.

_Component of:_
<br> PC Collider game objects

_Class output:_
<br> Calls methods on ModularPlayerController

_Class input:_
<br> Triggered by OnCollision Unity events

@author Jonny Aiello
@version 1.2.0
@date 5/22/17
*/ 
public class HitboxController_Template : MonoBehaviour {

	// Reference Variables
	public ModularPlayerController mpc;

	// [[ ----- ON COLLISION ENTER 2D ----- ]]
	private void OnCollisionEnter2D( Collision2D _c ){
		if( gameObject.activeSelf ){

		}
	}

	// [[ ----- ON COLLISION EXIT 2D ----- ]]
	private void OnCollisionExit2D( Collision2D _c ){
		if( gameObject.activeSelf ){

		}
	}

	// [[ ----- ON COLLISION STAY 2D ----- ]]
	private void OnCollisionStay2D( Collision2D _c ){
		if( gameObject.activeSelf ){
			
		}
	}

	// [[ ----- ON TRIGGER ENTER 2D ----- ]]
	private void OnTriggerEnter2D( Collider2D _c ){
		if( gameObject.activeSelf ){
			
		}
	}
}
}