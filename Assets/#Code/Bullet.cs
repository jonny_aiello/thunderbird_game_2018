﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace U2DPlatformer {

public class Bullet : MonoBehaviour {

	// Variables
	public Vector2 direction;
	public float speed;

	protected bool isActive;

	// Reference Variables
	public Transform sprite;

	// Properties
	public bool IsActive { get{return isActive;} }

// -----------------------------------------------------------------------------
// Unity Events

	// [[ ----- AWAKE ----- ]]
	private void Awake(){
		BulletController.pcBullets.Add(this); 
	}

	// Use this for initialization
	void Start () {
		// isActive = true;
	}
	
	// Update is called once per frame
	void Update () {
		if( isActive ){
			Vector2 moveVector = direction * (speed * Time.deltaTime); 
			transform.Translate( moveVector ); 
		}	
	}

	// [[ ----- ON TRIGGER ENTER 2D ----- ]]
	private void OnTriggerEnter2D( Collider2D _c ){
		if( isActive 
			&& _c.gameObject.layer != 8 
			&& _c.gameObject.layer != 10 ){

			Pool();
		}
		
	}

// -----------------------------------------------------------------------------
// Public Methods

	// [[ ----- FIRE ----- ]]
	public void Fire( Vector2 _position, Vector2 _dir, float _speed, PCS.Dir _facing ){
		if( !isActive ){
			sprite.rotation = PCS.GetQuatFromDir( _facing ); 
			isActive = true; 
			transform.position = _position; 
			direction = _dir;
			direction.Normalize();
			speed = _speed; 
		}
	}

	// [[ ----- POOL ----- ]]
	public void Pool(){
		if( isActive ){ 
			isActive = false; 
			transform.position = new Vector2( -10f, -10f ); 
		}
	}
}
}
